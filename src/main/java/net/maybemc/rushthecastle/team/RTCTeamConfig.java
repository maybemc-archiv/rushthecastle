package net.maybemc.rushthecastle.team;

import lombok.Getter;
import net.maybemc.aurelion.game.team.TeamConfig;

/**
 * @author Nico_ND1
 */
@Getter
public class RTCTeamConfig extends TeamConfig {
}
