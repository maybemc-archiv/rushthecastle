package net.maybemc.rushthecastle.team;

import net.maybemc.aurelion.game.team.Team;
import net.maybemc.quests.progress.ProgressType;
import net.maybemc.rushthecastle.actor.RTCActor;
import org.bukkit.Location;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;

/**
 * @author Nico_ND1
 */
public class RTCTeam extends Team<RTCActor, RTCTeam> {
    private int points;

    public RTCTeam(org.bukkit.@NotNull ChatColor legacyColor) {
        super(legacyColor.name(), legacyColor.asBungee(), legacyColor, 1);
    }

    @Override
    public @NotNull Location getSpawnLocation(@NotNull RTCActor rtcActor) {
        throw new UnsupportedOperationException();
    }

    public int getPoints() {
        return points;
    }

    public void addPoints(int points) {
        this.points += points;
        incrementStat("ingame-points");
        for (RTCActor actor : getActorsPlaying()) {
            actor.trackQuestProgress("ingame-points", points, ProgressType.INCREASE);
        }
    }

    @Override
    public String getDisplayNameFor(RTCActor actor) {
        Iterator<RTCActor> iterator = getActorsPlaying().iterator();
        if (iterator.hasNext()) {
            return iterator.next().getNickFor(actor).getDisplayName();
        }
        return super.getDisplayNameFor(actor);
    }
}
