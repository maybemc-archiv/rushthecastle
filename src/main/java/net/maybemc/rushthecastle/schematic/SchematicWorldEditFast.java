package net.maybemc.rushthecastle.schematic;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extension.platform.Actor;
import com.sk89q.worldedit.extension.platform.Locatable;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.math.transform.AffineTransform;
import com.sk89q.worldedit.math.transform.CombinedTransform;
import com.sk89q.worldedit.math.transform.Transform;
import com.sk89q.worldedit.session.ClipboardHolder;
import net.maybemc.aurelion.game.Games;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.protocol.result.Result;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;

import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;

/**
 * @author Nico_ND1
 */
public class SchematicWorldEditFast implements Schematic {
    private final String name;
    private final Clipboard clipboard;
    private final Transform transform;

    private SchematicWorldEditFast(String name, Clipboard clipboard, Transform transform) {
        this.name = name;
        this.clipboard = clipboard;
        this.transform = transform;
    }

    public SchematicWorldEditFast(String name, Clipboard clipboard) {
        this(name, clipboard, new CombinedTransform());
    }

    @Override
    public Schematic rotateTo(BlockFace direction) {
        // we (have to) assume that the schematic is facing NORTH by default!
        double rotation = direction.ordinal() * 90.0D;

        AffineTransform transform = new AffineTransform();
        transform = transform.rotateY(-rotation);
        //transform = transform.rotateX(0);
        //transform = transform.rotateZ(0);
        return new SchematicWorldEditFast(name, clipboard, transform);
    }

    @Override
    public CompletableFuture<Result> place(Location location, boolean includeAir) {
        return CompletableFuture.supplyAsync(() -> {
            try (EditSession editSession = WorldEdit.getInstance().newEditSession(new BukkitWorld(location.getWorld()))) {
                ClipboardHolder clipboardHolder = new ClipboardHolder(clipboard);
                clipboardHolder.setTransform(new CombinedTransform(clipboardHolder.getTransform(), transform));

                Operation operation = clipboardHolder
                    .createPaste(editSession)
                    .to(BlockVector3.at(location.getBlockX(), location.getBlockY(), location.getBlockZ()))
                    .ignoreAirBlocks(!includeAir)
                    .build();
                Operations.complete(operation);
                return Result.createOk();
            } catch (Exception e) {
                Games.getLogger().log(Level.SEVERE, "Error while pasting schematic " + name + " at " + location, e);
                return Result.createServerError();
            }
        }, Futures.asyncExecutor());
    }

    @Override
    public CompletableFuture<Result> place(Location location, Actor actor, boolean includeAir) {
        return CompletableFuture.supplyAsync(() -> {
            try (EditSession editSession = WorldEdit.getInstance().newEditSession((Actor & Locatable) actor)) {
                ClipboardHolder clipboardHolder = new ClipboardHolder(clipboard);
                clipboardHolder.setTransform(new CombinedTransform(clipboardHolder.getTransform(), transform));

                Operation operation = clipboardHolder
                    .createPaste(editSession)
                    .to(BlockVector3.at(location.getBlockX(), location.getBlockY(), location.getBlockZ()))
                    .ignoreAirBlocks(!includeAir)
                    .build();
                Operations.complete(operation);
                return Result.createOk();
            } catch (Exception e) {
                Games.getLogger().log(Level.SEVERE, "Error while pasting schematic " + name + " at " + location, e);
                return Result.createServerError();
            }
        }, Futures.asyncExecutor());
    }
}
