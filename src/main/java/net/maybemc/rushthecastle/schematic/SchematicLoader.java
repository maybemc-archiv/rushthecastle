package net.maybemc.rushthecastle.schematic;

/**
 * @author Nico_ND1
 */
public interface SchematicLoader {
    Schematic load(String name);
}
