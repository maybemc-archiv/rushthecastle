package net.maybemc.rushthecastle.schematic;

import com.sk89q.worldedit.extension.platform.Actor;
import net.maybemc.meybee.api.protocol.result.Result;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public interface Schematic {
    Schematic rotateTo(BlockFace direction);

    CompletableFuture<Result> place(Location location, boolean includeAir);

    default CompletableFuture<Result> place(Location location, Actor actor, boolean includeAir) {
        throw new UnsupportedOperationException();
    }
}
