package net.maybemc.rushthecastle.schematic;

import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormats;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Nico_ND1
 */
public class SchematicLoaderWorldEdit implements SchematicLoader {
    @Override
    public Schematic load(String name) {
        File file = new File("meybee/schematics/" + name + ".schem");
        ClipboardFormat format = ClipboardFormats.findByFile(file);

        try (ClipboardReader reader = format.getReader(new FileInputStream(file))) {
            Clipboard clipboard = reader.read();
            //return new SchematicWorldEdit(name, clipboard);
            return new SchematicWorldEditFast(name, clipboard);
        } catch (IOException exception) {
            throw new RuntimeException("Error loading schematic: " + name, exception);
        }
    }
}
