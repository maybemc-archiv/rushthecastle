package net.maybemc.rushthecastle.schematic;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.adapter.BukkitImplAdapter;
import com.sk89q.worldedit.entity.BaseEntity;
import com.sk89q.worldedit.entity.Entity;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.transform.BlockTransformExtent;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.math.Vector3;
import com.sk89q.worldedit.math.transform.AffineTransform;
import com.sk89q.worldedit.math.transform.CombinedTransform;
import com.sk89q.worldedit.math.transform.Transform;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldedit.world.block.BaseBlock;
import net.maybemc.aurelion.game.Games;
import net.maybemc.meybee.api.protocol.result.Result;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class SchematicWorldEdit implements Schematic {
    private final String name;
    private final Clipboard clipboard;
    private Transform transform;

    private BlockFace direction;

    public SchematicWorldEdit(String name, Clipboard clipboard) {
        this.name = name;
        this.clipboard = clipboard;
        this.direction = BlockFace.NORTH;
        this.transform = new CombinedTransform();
    }

    @Override
    public CompletableFuture<Result> place(Location location, boolean includeAir) {
        World worldEditWorld = new BukkitWorld(location.getWorld());
        try (EditSession editSession = WorldEdit.getInstance().newEditSession(worldEditWorld)) {
            List<Block> blocks = new ArrayList<>();
            placeBlocks(location, blocks, editSession, includeAir);

            List<org.bukkit.entity.Entity> entities = new ArrayList<>();
            spawnEntities(location, entities, editSession);
            return CompletableFuture.completedFuture(Result.createOk());
        } catch (MaxChangedBlocksException e) {
            throw new IllegalStateException("Error placing schematic " + name, e);
        }
    }

    @Override
    public Schematic rotateTo(BlockFace direction) {
        // we (have to) assume that the schematic is facing NORTH by default!
        double rotation = direction.ordinal() * 90.0D;

        AffineTransform transform = new AffineTransform();
        transform = transform.rotateY(-rotation);
        //transform = transform.rotateX(0);
        //transform = transform.rotateZ(0);
        this.transform = this.transform.combine(transform);
        this.direction = direction;
        return this;
    }

    private void placeBlocks(Location location, List<Block> blocks, EditSession editSession, boolean includeAir) throws MaxChangedBlocksException {
        BlockVector3 origin = clipboard.getOrigin();

        BlockVector3 min = clipboard.getMinimumPoint();
        BlockVector3 max = clipboard.getMaximumPoint();
        for (int x = min.getX(); x <= max.getX(); x++) {
            for (int y = min.getY(); y <= max.getY(); y++) {
                for (int z = min.getZ(); z <= max.getZ(); z++) {
                    BlockVector3 position = BlockVector3.at(x, y, z);
                    BaseBlock block = clipboard.getFullBlock(position);
                    if (!includeAir && block.getBlockType().getId().equals("minecraft:air")) {
                        continue;
                    }

                    block = BlockTransformExtent.transform(block, transform);

                    Vector3 transform = this.transform.apply(position.toVector3().subtract(origin.getX(), origin.getY(), origin.getZ()));
                    BlockVector3 actualPosition = BlockVector3.at(transform.getX() + location.getBlockX(), transform.getY() + location.getBlockY(), transform.getZ() + location.getBlockZ());
                    editSession.setBlock(actualPosition, block);

                    blocks.add(new Location(location.getWorld(), actualPosition.getX(), actualPosition.getY(), actualPosition.getZ()).getBlock());
                }
            }
        }
    }

    private void spawnEntities(Location location, List<org.bukkit.entity.Entity> entities, EditSession editSession) {
        BukkitImplAdapter weAdapter = getAdapter();

        BlockVector3 origin = clipboard.getOrigin();
        for (Entity entity : clipboard.getEntities()) {
            BaseEntity state = entity.getState();
            if (state == null) {
                Games.getLogger().info("Tried to spawn entity from schematic but state is null: " + entity);
                continue;
            }

            com.sk89q.worldedit.util.Location weLocation = entity.getLocation();
            Vector3 transform = this.transform.apply(weLocation.toVector().subtract(origin.getX(), origin.getY(), origin.getZ()));
            Vector3 directionTransform = this.transform.apply(weLocation.getDirection());
            Location entityLocation = new Location(location.getWorld(), transform.getX() + location.getX(), transform.getY() + location.getY(), transform.getZ() + location.getZ());
            entityLocation.setDirection(new Vector(directionTransform.getX(), directionTransform.getY(), directionTransform.getZ()));

            org.bukkit.entity.Entity bukkitEntity = weAdapter.createEntity(entityLocation, state);
            entities.add(bukkitEntity);
        }
    }

    private BukkitImplAdapter getAdapter() {
        WorldEditPlugin plugin = JavaPlugin.getPlugin(WorldEditPlugin.class);
        try {
            Method getMethod = plugin.getClass().getDeclaredMethod("getBukkitImplAdapter");
            getMethod.setAccessible(true);
            return (BukkitImplAdapter) getMethod.invoke(plugin);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
