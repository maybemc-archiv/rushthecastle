package net.maybemc.rushthecastle;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.json.GsonProvider;
import net.maybemc.meybee.api.stats.TimedStat;
import net.maybemc.meybee.spigot.stats.command.ConfigStatsCommandExecutor;
import net.maybemc.rushthecastle.item.RTCGsonItemStack;
import net.maybemc.rushthecastle.item.RTCItemStack;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.logging.Level;

/**
 * @author Nico_ND1
 */
public class RushTheCastlePlugin extends JavaPlugin {
    @Override
    public void onEnable() {
        GsonProvider gsonProvider = Meybee.getInstance().getProvider(GsonProvider.class);
        gsonProvider.registerConsumer(gsonBuilder -> {
            gsonBuilder.registerTypeAdapter(RTCItemStack.class, new RTCGsonItemStack());
        });

        RTCGame game;
        try {
            game = new RTCGame(this);
        } catch (IOException | InstantiationException | IllegalAccessException e) {
            getLogger().log(Level.SEVERE, "Error initializing game", e);
            Bukkit.shutdown();
            return;
        }

        Meybee.getInstance().registerProvider(RTCGame.class, game);
        game.boot();

        getCommand("stats").setExecutor(new ConfigStatsCommandExecutor("rushthecastle", TimedStat.LAST_30_DAYS, true));
        getCommand("statsall").setExecutor(new ConfigStatsCommandExecutor("rushthecastle", TimedStat.ALL_TIME, true));
        getCommand("stats7d").setExecutor(new ConfigStatsCommandExecutor("rushthecastle", TimedStat.LAST_7_DAYS, true));

        getCommand("buildingmenu").setExecutor(new NoneCommandExecutor(game));
    }
}
