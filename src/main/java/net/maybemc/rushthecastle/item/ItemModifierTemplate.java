package net.maybemc.rushthecastle.item;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.maybemc.rushthecastle.actor.RTCActor;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Getter
public class ItemModifierTemplate implements ItemModifier, ConfigurationSerializable {
    @Override
    public ItemStack modify(ItemStack input, RTCActor actor) {
        ItemMeta itemMeta = input.getItemMeta();
        itemMeta.setDisplayName(actor.translate("x-name"));
        itemMeta.setLore(Arrays.asList(actor.translate("x-lore").split("\n")));

        // set persistent data
        PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();

        input.setItemMeta(itemMeta);
        return input;
    }

    public static ItemModifierTemplate deserialize(Map<String, Object> map) {
        return new ItemModifierTemplate();
    }

    public static ItemModifierTemplate deserialize(JsonObject object) {
        return new ItemModifierTemplate();
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        return map;
    }
}
