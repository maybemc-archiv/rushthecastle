package net.maybemc.rushthecastle.item;

import me.lucko.helper.item.ItemStackBuilder;
import net.maybemc.aurelion.game.property.PropertyKey;
import net.maybemc.aurelion.game.state.item.GameStateItem;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author Nico_ND1
 */
public class PlayerHiderItem extends GameStateItem<RTCActor, RTCTeam> {
    private static final PropertyKey<Boolean> HIDDEN = PropertyKey.create("hidden", Boolean.class);

    private final RTCGame game;

    public PlayerHiderItem(RTCGame game) {
        super("player-hider", 2);
        this.game = game;
    }

    @Override
    public ItemStack getItemStack(RTCActor actor) {
        boolean hasHidden = actor.getProperty(HIDDEN, false);
        String key = hasHidden ? "item-player-hider-hidden" : "item-player-hider-visible";
        return ItemStackBuilder.of(hasHidden ? Material.GRAY_DYE : Material.LIME_DYE)
            .name(actor.translate(key + "-name"))
            .lore(actor.translate(key + "-lore").split("\n"))
            .build();
    }

    @Override
    public void onInteract(RTCActor actor, PlayerInteractEvent event) {
        boolean hasHidden = actor.getProperty(HIDDEN, false);
        if (hasHidden) {
            actor.setProperty(HIDDEN, false);

            for (RTCActor rtcActor : game.getActorsPlaying()) {
                actor.getPlayer().showPlayer(game.getPlugin(), rtcActor.getPlayer());
            }
        } else {
            actor.setProperty(HIDDEN, true);

            for (RTCActor rtcActor : game.getActorsPlaying()) {
                actor.getPlayer().hidePlayer(game.getPlugin(), rtcActor.getPlayer());
            }
        }

        actor.playSound(Sound.ITEM_ARMOR_EQUIP_ELYTRA);
        forceGive(actor);
    }
}
