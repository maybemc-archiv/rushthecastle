package net.maybemc.rushthecastle.item;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.JarEffectMapObject;
import net.maybemc.rushthecastle.map.object.MapObject;
import net.maybemc.rushthecastle.map.object.MapObjectType;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Getter
public class EffectMapObjectItemModifier implements ItemModifier, MapObjectCreator, ConfigurationSerializable {
    private int amplifier;
    private int duration;
    private PotionEffectType effectType;
    private boolean ambient;
    private boolean particles;
    private boolean icon;

    @Override
    public MapObject create(RTCGame game, Castle castle, Location location) {
        return new JarEffectMapObject(game, castle, location.getBlock(), new PotionEffect(effectType, duration, amplifier, ambient, particles, icon));
    }

    @Override
    public MapObjectType getType() {
        return MapObjectType.EFFECT;
    }

    @Override
    public ItemStack modify(ItemStack input, RTCActor actor) {
        ItemMeta itemMeta = input.getItemMeta();
        itemMeta.setDisplayName(actor.translate("map-object-item-" + MapObjectType.EFFECT.name() + "-" + effectType.getName() + "-name"));

        // set persistent data
        PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();
        dataContainer.set(ItemDataTypes.EFFECT_MAP_OBJECT_KEY, ItemDataTypes.EFFECT_MAP_OBJECT_TYPE, this);

        input.setItemMeta(itemMeta);
        return input;
    }

    public static EffectMapObjectItemModifier deserialize(Map<String, Object> map) {
        return new EffectMapObjectItemModifier(
            (int) map.get("amplifier"),
            (int) map.get("duration"),
            PotionEffectType.getByName((String) map.get("effectType")),
            (boolean) map.get("ambient"),
            (boolean) map.get("particles"),
            (boolean) map.get("icon")
        );
    }

    public static EffectMapObjectItemModifier deserialize(JsonObject object) {
        return new EffectMapObjectItemModifier(
            object.get("amplifier").getAsInt(),
            object.get("duration").getAsInt(),
            PotionEffectType.getByName(object.get("effectType").getAsString()),
            object.get("ambient").getAsBoolean(),
            object.get("particles").getAsBoolean(),
            object.get("icon").getAsBoolean()
        );
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("amplifier", amplifier);
        map.put("duration", duration);
        map.put("effectType", effectType.getName());
        map.put("ambient", ambient);
        map.put("particles", particles);
        map.put("icon", icon);
        return map;
    }
}
