package net.maybemc.rushthecastle.item;

import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.MapObject;
import net.maybemc.rushthecastle.map.object.MapObjectType;
import org.bukkit.Location;

/**
 * @author Nico_ND1
 */
public interface MapObjectCreator {
    MapObject create(RTCGame game, Castle castle, Location location);

    MapObjectType getType();
}
