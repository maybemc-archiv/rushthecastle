package net.maybemc.rushthecastle.item;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.maybemc.rushthecastle.actor.RTCActor;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nico_ND1
 */
@Getter
@AllArgsConstructor
public class SchematicPlacerItemModifier implements ItemModifier, ConfigurationSerializable {
    private String schematicName;

    @Override
    public ItemStack modify(ItemStack input, RTCActor actor) {
        ItemMeta itemMeta = input.getItemMeta();
        itemMeta.setDisplayName(actor.translate("schematic-placer-" + schematicName + "-name"));
        itemMeta.setLore(Arrays.asList(actor.translate("schematic-placer-" + schematicName + "-lore").split("\n")));

        // set persistent data
        PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();
        dataContainer.set(ItemDataTypes.SCHEMATIC_PLACER_KEY, ItemDataTypes.SCHEMATIC_PLACER_TYPE, this);

        input.setItemMeta(itemMeta);
        return input;
    }

    public static SchematicPlacerItemModifier deserialize(Map<String, Object> map) {
        return new SchematicPlacerItemModifier(
            (String) map.get("schematicName")
        );
    }

    public static SchematicPlacerItemModifier deserialize(JsonObject object) {
        return new SchematicPlacerItemModifier(
            object.get("schematicName").getAsString()
        );
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("schematicName", schematicName);
        return map;
    }
}
