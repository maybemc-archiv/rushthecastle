package net.maybemc.rushthecastle.item;

import net.maybemc.rushthecastle.actor.RTCActor;
import org.bukkit.inventory.ItemStack;

/**
 * @author Nico_ND1
 */
public interface ItemModifier extends Cloneable {
    default ItemStack createDefaultItem() {
        throw new UnsupportedOperationException();
    }

    ItemStack modify(ItemStack input, RTCActor actor);
}
