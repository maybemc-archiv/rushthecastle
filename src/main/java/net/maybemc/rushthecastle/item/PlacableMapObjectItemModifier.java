package net.maybemc.rushthecastle.item;

import com.google.common.base.Preconditions;
import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.MapObject;
import net.maybemc.rushthecastle.map.object.MapObjectType;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Getter
public class PlacableMapObjectItemModifier implements ItemModifier, MapObjectCreator, ConfigurationSerializable {
    private MapObjectType type;

    @Override
    public MapObject create(RTCGame game, Castle castle, Location location) {
        Preconditions.checkArgument(type != MapObjectType.EFFECT, "Cannot create effect map object");

        try {
            Class<? extends MapObject> clazz = type.getClazz();
            return clazz.getConstructor(RTCGame.class, Castle.class, Block.class).newInstance(game, castle, location.getBlock());
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException |
                 NoSuchMethodException e) {
            throw new RuntimeException("Error creating map object for " + type.name(), e);
        }
    }

    @Override
    public ItemStack modify(ItemStack input, RTCActor actor) {
        ItemMeta itemMeta = input.getItemMeta();
        itemMeta.setDisplayName(actor.translate("map-object-item-" + type.name() + "-name"));
        String loreRaw = actor.translate("map-object-item-" + type.name() + "-lore");
        if (!"-EMPTY-".equals(loreRaw)) {
            itemMeta.setLore(Arrays.asList(loreRaw.split("\n")));
        }

        // set persistent data
        PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();
        dataContainer.set(ItemDataTypes.PLACABLE_MAP_OBJECT_KEY, ItemDataTypes.PLACABLE_MAP_OBJECT_TYPE, this);

        input.setItemMeta(itemMeta);
        return input;
    }

    public static PlacableMapObjectItemModifier deserialize(Map<String, Object> map) {
        return new PlacableMapObjectItemModifier(MapObjectType.valueOf(map.get("type").toString()));
    }

    public static PlacableMapObjectItemModifier deserialize(JsonObject object) {
        return new PlacableMapObjectItemModifier(MapObjectType.valueOf(object.get("type").getAsString()));
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", type.name());
        return map;
    }
}
