package net.maybemc.rushthecastle.item;

import me.lucko.helper.item.ItemStackBuilder;
import net.maybemc.aurelion.game.state.item.GameStateItem;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author Nico_ND1
 */
public class CastleUnfinishItem extends GameStateItem<RTCActor, RTCTeam> {
    public CastleUnfinishItem() {
        super("castle-unfinish", 4);
    }

    @Override
    public boolean shouldGiveItem(RTCActor actor) {
        Castle ownCastle = actor.getOwnCastle();
        if (ownCastle != null) {
            return ownCastle.isFinished();
        }
        return false;
    }

    @Override
    public ItemStack getItemStack(RTCActor actor) {
        return ItemStackBuilder.of(Material.NAME_TAG)
            .name(actor.translate("castle-unfinish-item-name"))
            .lore(actor.translate("castle-unfinish-item-lore").split("\n"))
            .build();
    }

    @Override
    public void onInteract(RTCActor actor, PlayerInteractEvent event) {
        actor.unfinishBuilding();
    }
}
