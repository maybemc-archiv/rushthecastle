package net.maybemc.rushthecastle.item;

import net.maybemc.rushthecastle.actor.RTCActor;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Random;

/**
 * @author Nico_ND1
 */
public class RTCItemStack {
    private static final Random RANDOM = new Random();

    private final List<ItemStack> items;
    private final List<ItemModifier> itemModifiers;

    public RTCItemStack(List<ItemStack> items, List<ItemModifier> itemModifiers) {
        this.items = items;
        this.itemModifiers = itemModifiers;
    }

    public List<ItemStack> getItems() {
        return items;
    }

    public ItemStack getRandomRawItem() {
        return items.get(RANDOM.nextInt(items.size()));
    }

    public ItemStack getItemStack(RTCActor viewer) {
        ItemStack itemStack = getRandomRawItem().clone();
        for (ItemModifier itemModifier : itemModifiers) {
            itemStack = itemModifier.modify(itemStack, viewer);
        }
        return itemStack;
    }
}
