package net.maybemc.rushthecastle.item;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Nico_ND1
 */
public class RTCGsonItemStack implements JsonDeserializer<RTCItemStack> {
    @Override
    public RTCItemStack deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject configuration = json.getAsJsonObject();
        List<ItemStack> items = new ArrayList<>();
        if (configuration.get("itemStack").isJsonPrimitive()) {
            items.add(new ItemStack(Material.valueOf(configuration.get("itemStack").getAsString())));
        } else if (configuration.get("itemStack").isJsonArray()) {
            for (JsonElement element : configuration.get("itemStack").getAsJsonArray()) {
                items.add(context.deserialize(element, ItemStack.class));
            }
        } else {
            items.add(context.deserialize(configuration.get("itemStack"), ItemStack.class));
        }

        List<ItemModifier> itemModifiers = Collections.emptyList();
        for (ItemStack itemStack : items) {
            itemModifiers = new ArrayList<>();

            ItemMeta itemMeta = itemStack.getItemMeta();
            PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();

            if (configuration.has("placableMapObject")) {
                PlacableMapObjectItemModifier signature = PlacableMapObjectItemModifier.deserialize(configuration.get("placableMapObject").getAsJsonObject());
                dataContainer.set(ItemDataTypes.PLACABLE_MAP_OBJECT_KEY, ItemDataTypes.PLACABLE_MAP_OBJECT_TYPE, signature);
                itemModifiers.add(signature);
            }

            if (configuration.has("effectMapObject")) {
                EffectMapObjectItemModifier signature = EffectMapObjectItemModifier.deserialize(configuration.get("effectMapObject").getAsJsonObject());
                dataContainer.set(ItemDataTypes.EFFECT_MAP_OBJECT_KEY, ItemDataTypes.EFFECT_MAP_OBJECT_TYPE, signature);
                itemModifiers.add(signature);
            }

            if (configuration.has("schematicPlacer")) {
                SchematicPlacerItemModifier signature = SchematicPlacerItemModifier.deserialize(configuration.get("schematicPlacer").getAsJsonObject());
                dataContainer.set(ItemDataTypes.SCHEMATIC_PLACER_KEY, ItemDataTypes.SCHEMATIC_PLACER_TYPE, signature);
                itemModifiers.add(signature);
            }

            itemStack.setItemMeta(itemMeta);
        }
        return new RTCItemStack(items, itemModifiers);
    }
}
