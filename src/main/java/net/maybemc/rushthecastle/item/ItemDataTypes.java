package net.maybemc.rushthecastle.item;

import com.jeff_media.morepersistentdatatypes.datatypes.serializable.ConfigurationSerializableDataType;
import net.maybemc.rushthecastle.RushTheCastlePlugin;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;

/**
 * @author Nico_ND1
 */
public class ItemDataTypes {
    public static final NamespacedKey PLACABLE_MAP_OBJECT_KEY = NamespacedKey.fromString("placable-map-object", JavaPlugin.getPlugin(RushTheCastlePlugin.class));
    public static final PersistentDataType<byte[], PlacableMapObjectItemModifier> PLACABLE_MAP_OBJECT_TYPE = new ConfigurationSerializableDataType<>(PlacableMapObjectItemModifier.class);

    public static final NamespacedKey EFFECT_MAP_OBJECT_KEY = NamespacedKey.fromString("effect-map-object", JavaPlugin.getPlugin(RushTheCastlePlugin.class));
    public static final PersistentDataType<byte[], EffectMapObjectItemModifier> EFFECT_MAP_OBJECT_TYPE = new ConfigurationSerializableDataType<>(EffectMapObjectItemModifier.class);

    public static final NamespacedKey SCHEMATIC_PLACER_KEY = NamespacedKey.fromString("schematic-placer", JavaPlugin.getPlugin(RushTheCastlePlugin.class));
    public static final PersistentDataType<byte[], SchematicPlacerItemModifier> SCHEMATIC_PLACER_TYPE = new ConfigurationSerializableDataType<>(SchematicPlacerItemModifier.class);

    public static Optional<MapObjectCreator> findMapObjectCreator(PersistentDataContainer container) {
        if (container.has(PLACABLE_MAP_OBJECT_KEY, PLACABLE_MAP_OBJECT_TYPE)) {
            return Optional.of(container.get(PLACABLE_MAP_OBJECT_KEY, PLACABLE_MAP_OBJECT_TYPE));
        } else if (container.has(EFFECT_MAP_OBJECT_KEY, EFFECT_MAP_OBJECT_TYPE)) {
            return Optional.of(container.get(EFFECT_MAP_OBJECT_KEY, EFFECT_MAP_OBJECT_TYPE));
        } else {
            return Optional.empty();
        }
    }

    static {
        ConfigurationSerialization.registerClass(PlacableMapObjectItemModifier.class);
        ConfigurationSerialization.registerClass(EffectMapObjectItemModifier.class);
        ConfigurationSerialization.registerClass(SchematicPlacerItemModifier.class);
    }
}
