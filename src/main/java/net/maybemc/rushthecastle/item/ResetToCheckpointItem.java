package net.maybemc.rushthecastle.item;

import me.lucko.helper.item.ItemStackBuilder;
import net.maybemc.aurelion.game.state.item.GameStateItem;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author Nico_ND1
 */
public class ResetToCheckpointItem extends GameStateItem<RTCActor, RTCTeam> {
    public ResetToCheckpointItem() {
        super("reset-to-checkpoint", 4);
    }

    @Override
    public ItemStack getItemStack(RTCActor actor) {
        return ItemStackBuilder.of(Material.HEART_OF_THE_SEA)
            .name(actor.translate("item-reset-to-checkpoint-name"))
            .lore(actor.translate("item-reset-to-checkpoint-lore").split("\n"))
            .build();
    }

    @Override
    public void onInteract(RTCActor actor, PlayerInteractEvent event) {
        actor.operateSession(jarSession -> jarSession.resetToCheckpoint(actor));
    }
}
