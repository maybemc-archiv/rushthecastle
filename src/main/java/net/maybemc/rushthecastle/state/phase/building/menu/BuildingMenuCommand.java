package net.maybemc.rushthecastle.state.phase.building.menu;

import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.state.phase.building.BuildingGamePhase;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

/**
 * @author Nico_ND1
 */
public class BuildingMenuCommand implements CommandExecutor {
    private final BuildingGamePhase gamePhase;

    public BuildingMenuCommand(BuildingGamePhase gamePhase) {
        this.gamePhase = gamePhase;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        RTCActor actor = gamePhase.getGame().getActor((Player) sender);
        gamePhase.tryGiveItems(actor);
        return true;
    }
}
