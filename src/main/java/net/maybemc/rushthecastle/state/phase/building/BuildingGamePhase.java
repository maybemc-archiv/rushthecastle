package net.maybemc.rushthecastle.state.phase.building;

import net.maybemc.aurelion.game.Games;
import net.maybemc.aurelion.game.actor.PlayState;
import net.maybemc.aurelion.game.state.countdown.CountdownConfiguration;
import net.maybemc.aurelion.game.state.countdown.CountdownHandler;
import net.maybemc.aurelion.game.state.endlogic.BiEndLogic;
import net.maybemc.aurelion.game.state.endlogic.CountdownEndLogic;
import net.maybemc.aurelion.game.state.listener.GameStateEventCanceller;
import net.maybemc.aurelion.game.state.phase.GamePhase;
import net.maybemc.rushthecastle.NoneCommandExecutor;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.RewardConfig;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.item.CastleUnfinishItem;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.MapObjectGameStateListener;
import net.maybemc.rushthecastle.map.object.MapObjectMoveListener;
import net.maybemc.rushthecastle.map.object.MapObjectTickListener;
import net.maybemc.rushthecastle.state.RedstoneListener;
import net.maybemc.rushthecastle.state.phase.building.menu.BuildingMenuCommand;
import net.maybemc.rushthecastle.state.phase.building.menu.BuildingMenuItem;
import net.maybemc.rushthecastle.state.phase.building.scoreboard.BuildingScoreboardHandler;
import net.maybemc.rushthecastle.state.phase.building.scoreboard.BuildingScoreboardListener;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author Nico_ND1
 */
public class BuildingGamePhase extends GamePhase<RTCActor, RTCTeam, RTCGameMap, RTCGame> implements CountdownHandler {
    private final BuildingScoreboardHandler scoreboardHandler;

    public BuildingGamePhase(RTCGame game) {
        super(game, new BiEndLogic<>(new EveryoneFinishedBuildingEndLogic(), new CountdownEndLogic<>()));

        this.scoreboardHandler = new BuildingScoreboardHandler(game);
        registerListener(new BuildingScoreboardListener(game, this.scoreboardHandler));

        registerBukkitListener(GameStateEventCanceller.builder()
            .itemPickup(false)
            .interactEntity(false)
            .foodChange(false)
            .weather(false)
            .explosions(false)
            .entityDamage(false)
            .build());

        registerListener(new MapObjectGameStateListener(game));
        registerListener(new MapObjectMoveListener(game));
        registerListener(new MapObjectTickListener(game));
        registerListener(new BuildingListener(game));
        registerListener(new RedstoneListener(game));
        registerListener(new SchematicPlacerListener(game));

        registerItem(new CastleUnfinishItem());
        registerItem(new BuildingMenuItem(game.getBuildingMenuConfig()));
    }

    @Override
    public void handleQuit(@NotNull RTCActor actor) {
        scoreboardHandler.updatePlayers();
    }

    @Override
    public void onJoin() {
        game.enableCosmeticGG();

        for (RTCActor actor : game.getActorsPlaying()) {
            actor.clearPlayer();
            actor.getPlayer().setCollidable(false);

            Castle ownCastle = actor.getOwnCastle();
            if (ownCastle == null) {
                Games.getLogger().warning("Player " + actor.getPlayer().getName() + " has no own castle, will remove him from playing");
                actor.setPlayState(PlayState.SPECTATOR);
                continue;
            }

            ownCastle.teleport(actor);
            tryGiveItems(actor);

            actor.getPlayer().setAllowFlight(true);
            actor.reward(RewardConfig.Action.BUILDING_PHASE_PARTICIPATE);

            actor.sendMessage("building-phase-started");
            actor.sendTitle("building-phase-started-title", "building-phase-started-subtitle", 0, 50, 12);
            actor.incrementStat("participate-building");
        }

        scoreboardHandler.setup();

        game.getPlugin().getCommand("buildingmenu").setExecutor(new BuildingMenuCommand(this));
    }

    @Override
    public void onQuit() {
        for (RTCActor actor : game.getActorsPlaying()) {
            actor.clearPlayer();

            Castle ownCastle = actor.getOwnCastle();
            if (ownCastle == null) {
                Games.getLogger().warning("Player " + actor.getPlayer().getName() + " has no own castle, will remove him from playing");
                actor.setPlayState(PlayState.SPECTATOR);
                continue;
            }

            if (ownCastle.findFinishObject().isEmpty()) {
                actor.sendMessage("building-phase-end-no-finish-object");
                actor.setPlayState(PlayState.SPECTATOR);
                continue;
            }
        }

        game.getPlugin().getCommand("buildingmenu").setExecutor(new NoneCommandExecutor(game));
    }

    @Override
    public @NotNull Location getSpawnPoint(@Nullable RTCActor actor) {
        if (actor == null) {
            return game.getMap().getWorld().getSpawnLocation();
        }
        return actor.getOwnCastle().getSpawnLocation();
    }

    @Override
    public @NotNull CountdownConfiguration getConfiguration() {
        return new CountdownConfiguration(game.getBuildingTime(), CountdownConfiguration.Direction.DOWN);
    }

    @Override
    public void onTick(int currentTick) {
        scoreboardHandler.updateTime();

        if (currentTick % 60 == 0 || currentTick == 30 || currentTick == 20 || currentTick <= 10) {
            if (currentTick > 60) {
                game.sendMessage("building-phase-tick-minutes", currentTick / 60);
            } else {
                game.sendMessage("building-phase-tick", currentTick);
            }

            if (currentTick <= 60) {
                game.getActors().forEach(actor -> actor.playSound(Sound.BLOCK_NOTE_BLOCK_BASS));
            }
        }
    }

    @Override
    public void onStop(@NotNull StopReason reason) {
        if (reason == StopReason.FINISHED) {
            game.tryEnd();
        }
    }
}
