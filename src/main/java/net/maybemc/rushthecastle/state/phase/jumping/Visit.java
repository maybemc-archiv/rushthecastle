package net.maybemc.rushthecastle.state.phase.jumping;

import lombok.Getter;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.castle.Castle;

/**
 * @author Nico_ND1
 */
@Getter
public class Visit {
    private final RTCActor owner;
    private final Castle castle;
    private long bestTime;

    public Visit(RTCActor owner, Castle castle) {
        this.owner = owner;
        this.castle = castle;
        this.bestTime = owner.getTestTime();
    }
}
