package net.maybemc.rushthecastle.state.phase.testing.scoreboard;

import net.maybemc.aurelion.game.Games;
import net.maybemc.aurelion.game.state.GameState;
import net.maybemc.aurelion.game.state.PlayingGameState;
import net.maybemc.aurelion.game.state.countdown.Countdown;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.maybemc.meybee.spigot.scoreboard.ScoreboardLine;
import net.maybemc.meybee.spigot.scoreboard.UserScoreboard;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.theme.Theme;
import net.maybemc.rushthecastle.state.phase.testing.PlacementTracker;
import net.maybemc.rushthecastle.team.RTCTeam;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.ToIntFunction;

/**
 * @author Nico_ND1
 */
public class TestingScoreboardHandler {
    private final RTCGame game;
    private final PlacementTracker placementTracker;
    private int finishLineHeight;

    public TestingScoreboardHandler(RTCGame game, PlacementTracker placementTracker) {
        this.game = game;
        this.placementTracker = placementTracker;
    }

    public void setup() {
        for (RTCActor actor : game.getActors()) {
            setup(actor);
        }
    }

    public void setup(RTCActor actor) {
        Collection<RTCActor> actorsPlaying = game.getActorsPlaying();

        actor.resetScoreboard();

        UserScoreboard scoreboard = actor.getScoreboard();
        scoreboard.setTitle(actor.translate("testing-phase-scoreboard-title"));

        int lineHeight = actorsPlaying.size() + 7;

        scoreboard.createEmptyLines(lineHeight--);
        scoreboard.createLine("time-header", lineHeight--).setText(actor.translate("testing-phase-scoreboard-time-header"));
        scoreboard.createLine("time-value", lineHeight--);
        scoreboard.createEmptyLines(lineHeight--);
        updateTime();

        scoreboard.createLine("theme-header", lineHeight--).setText(actor.translate("testing-phase-scoreboard-theme-header"));
        scoreboard.createLine("theme-value", lineHeight--);
        scoreboard.createEmptyLines(lineHeight--);
        setTheme(actor, scoreboard);

        this.finishLineHeight = lineHeight;

        for (RTCActor rtcActor : actorsPlaying) {
            Nick nick = rtcActor.getNickFor(actor);
            boolean self = rtcActor.equals(actor);
            scoreboard.createLine("actor-" + rtcActor.getId(), 0).setText(actor.translate("testing-phase-scoreboard-player" + (self ? "-self" : ""), nick.getDisplayName()));
        }
    }

    public void updateTime() {
        int secondsLeft = 0;

        GameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> currentState = game.getCurrentState();
        if (currentState instanceof PlayingGameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> playingGameState) {
            GameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> currentPhase = playingGameState.getCurrentPhase();
            Countdown countdown = currentPhase.getCountdown();
            secondsLeft = countdown.getRemainingTime();
        }

        String formattedString = String.format("%02d:%02d", secondsLeft / 60, secondsLeft % 60);
        for (RTCActor actor : game.getActors()) {
            actor.getScoreboard().getLine("time-value").setText(actor.translate("testing-phase-scoreboard-time-value", formattedString));
        }
    }

    private void setTheme(RTCActor actor, UserScoreboard scoreboard) {
        Castle ownCastle = actor.getOwnCastle();
        Theme theme = ownCastle.getTheme();
        String themeName = theme.getCosmetic().translateName(actor.getUser(), game.getLanguagePack());

        scoreboard.getLine("theme-value").setText(actor.translate("testing-phase-scoreboard-theme-value", themeName));
    }

    public void onFinish(RTCActor actor) {
        int placement = placementTracker.getPlacement(actor);
        for (RTCActor gameActor : game.getActors()) {
            Nick nick = actor.getNickFor(gameActor);
            UserScoreboard scoreboard = gameActor.getScoreboard();
            ScoreboardLine line = scoreboard.getLine("actor-" + actor.getId());
            line.setLine(finishLineHeight--);

            boolean self = gameActor.equals(actor);
            line.setText(gameActor.translate("testing-phase-scoreboard-player-finished" + (self ? "-self" : ""), placement, nick.getDisplayName()));

            reorder(scoreboard);
        }
    }

    private void reorder(UserScoreboard scoreboard) {
        int lineHeight = this.finishLineHeight;

        List<RTCActor> actors = game.getActorsPlaying().stream()
            .filter(actor -> placementTracker.getPlacementLazy(actor).isPresent())
            .sorted(Comparator.comparingInt((ToIntFunction<RTCActor>) value -> placementTracker.getPlacementLazy(value).get()).reversed())
            .toList();
        for (RTCActor actor : actors) {
            ScoreboardLine line = scoreboard.getLine("actor-" + actor.getId());
            if (line != null) {
                line.setLine(lineHeight--);
            } else {
                Games.getLogger().warning("Could not find line for actor " + actor.getId() + " in scoreboard");
            }
        }
    }

    public void remove(RTCActor actor) {
        for (RTCActor gameActor : game.getActors()) {
            UserScoreboard scoreboard = gameActor.getScoreboard();
            ScoreboardLine line = scoreboard.getLine("actor-" + actor.getId());
            line.remove();
        }
    }
}
