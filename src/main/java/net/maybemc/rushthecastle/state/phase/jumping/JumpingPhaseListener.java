package net.maybemc.rushthecastle.state.phase.jumping;

import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.RewardConfig;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.CastleFinishEvent;
import net.maybemc.rushthecastle.state.phase.testing.PlacementTracker;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;

/**
 * @author Nico_ND1
 */
public class JumpingPhaseListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private final JumpingPhase jumpingPhase;
    private final PlacementTracker placementTracker;

    public JumpingPhaseListener(RTCGame game, JumpingPhase jumpingPhase, PlacementTracker placementTracker) {
        super(game);
        this.jumpingPhase = jumpingPhase;
        this.placementTracker = placementTracker;
    }

    @EventHandler
    public void onFinish(CastleFinishEvent event) {
        RTCActor actor = event.getActor();
        int placement = placementTracker.getPlacement(actor);
        int points = game.getPointsConfig().getPoints(placement);
        actor.getTeam().addPoints(points);
        if (points > 0) {
            actor.sendMessage("points-earned", points, placement);
        }

        actor.clearPlayer();
        actor.makeSpectatorTemporary();

        for (RTCActor gameActor : game.getActors()) {
            Nick nick = actor.getNickFor(gameActor);
            gameActor.sendMessage("jumping-phase-finish-reached", nick.getDisplayName(), placement, points);
        }

        actor.playSound(Sound.ENTITY_PLAYER_LEVELUP);
        actor.reward(RewardConfig.Action.FINISH_SESSION_FOR_VISITED_CASTLE);
        actor.reward(RewardConfig.Action.SESSION_PLACEMENT, Integer.toString(placement));

        if (jumpingPhase.isCurrentVisitFinished()) {
            jumpingPhase.nextVisit();
        } else {
            game.tryEnd();
        }
    }
}
