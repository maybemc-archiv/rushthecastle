package net.maybemc.rushthecastle.state.phase.frozen;

import net.maybemc.aurelion.game.state.countdown.CountdownConfiguration;
import net.maybemc.aurelion.game.state.countdown.CountdownHandler;
import net.maybemc.aurelion.game.state.endlogic.CountdownEndLogic;
import net.maybemc.aurelion.game.state.listener.GameStateEventCanceller;
import net.maybemc.aurelion.game.state.phase.GamePhase;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.object.MapObjectTickListener;
import net.maybemc.rushthecastle.state.RedstoneListener;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author Nico_ND1
 */
public class FrozenGamePhase extends GamePhase<RTCActor, RTCTeam, RTCGameMap, RTCGame> implements CountdownHandler {
    private final GamePhase<RTCActor, RTCTeam, RTCGameMap, RTCGame> followingPhase;

    public FrozenGamePhase(RTCGame game, GamePhase<RTCActor, RTCTeam, RTCGameMap, RTCGame> followingPhase) {
        super(game, new CountdownEndLogic<>());
        this.followingPhase = followingPhase;

        registerListener(new FrozenPhaseListener(game));

        registerBukkitListener(GameStateEventCanceller.builder()
            .itemDrop(false)
            .itemPickup(false)
            .interactAir(false)
            .interactEntity(false)
            .interactPhysical(false)
            .interactBlock(false)
            .hangingBreak(false)
            .foodChange(false)
            .weather(false)
            .explosions(false)
            .entityDamage(false)
            .armorStandManipulate(false)
            .build());

        registerListener(new RedstoneListener(game));
        registerListener(new MapObjectTickListener(game));
    }

    @Override
    public @Nullable GamePhase<RTCActor, RTCTeam, RTCGameMap, RTCGame> followingGamePhase() {
        return this.followingPhase;
    }

    @Override
    public void handleQuit(@NotNull RTCActor actor) {
    }

    @Override
    public void onJoin() {
        for (RTCActor actor : game.getActorsPlaying()) {
            actor.clearPlayer();
            actor.getPlayer().teleport(getSpawnPoint(actor));
        }
    }

    @Override
    public void onQuit() {
    }

    @Override
    public @NotNull Location getSpawnPoint(@Nullable RTCActor actor) {
        return followingPhase.getSpawnPoint(actor);
    }

    @Override
    public @NotNull CountdownConfiguration getConfiguration() {
        return new CountdownConfiguration(3, CountdownConfiguration.Direction.DOWN);
    }

    @Override
    public void onTick(int i) {
        if (i % 10 == 0 || i <= 5) {
            for (RTCActor actor : game.getActors()) {
                actor.sendTitle("frozen-phase-tick-title", "frozen-phase-tick-subtitle", 0, 20, 5, i);
                actor.playSound(Sound.BLOCK_NOTE_BLOCK_BASS);
            }
        }
    }

    @Override
    public void onStop(@NotNull StopReason reason) {
        game.tryEnd();
    }
}
