package net.maybemc.rushthecastle.state.phase.frozen;

import net.maybemc.aurelion.game.state.listener.BukkitActorEventHandler;
import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Location;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author Nico_ND1
 */
public class FrozenPhaseListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    public FrozenPhaseListener(RTCGame game) {
        super(game);
    }

    @BukkitActorEventHandler(ignoreSpectator = true)
    public void onMove(RTCActor actor, PlayerMoveEvent event) {
        Location from = event.getFrom();
        Location to = event.getTo();

        if (from.getBlockX() != to.getBlockX() || from.getBlockZ() != to.getBlockZ()) {
            Location location = actor.getPlayer().getLocation();
            from.setYaw(location.getYaw());
            from.setPitch(location.getPitch());
            actor.getPlayer().teleport(from);
        }
    }
}
