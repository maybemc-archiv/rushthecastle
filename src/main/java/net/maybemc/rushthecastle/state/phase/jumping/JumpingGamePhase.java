package net.maybemc.rushthecastle.state.phase.jumping;

import net.maybemc.aurelion.game.actor.PlayState;
import net.maybemc.aurelion.game.state.countdown.CountdownConfiguration;
import net.maybemc.aurelion.game.state.countdown.CountdownHandler;
import net.maybemc.aurelion.game.state.listener.GameStateEventCanceller;
import net.maybemc.aurelion.game.state.phase.GamePhase;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.item.PlayerHiderItem;
import net.maybemc.rushthecastle.item.ResetToCheckpointItem;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.MapObjectMoveListener;
import net.maybemc.rushthecastle.map.object.MapObjectTickListener;
import net.maybemc.rushthecastle.state.RedstoneListener;
import net.maybemc.rushthecastle.state.phase.frozen.FrozenGamePhase;
import net.maybemc.rushthecastle.state.phase.jumping.scoreboard.JumpingScoreboardHandler;
import net.maybemc.rushthecastle.state.phase.jumping.scoreboard.JumpingScoreboardListener;
import net.maybemc.rushthecastle.state.phase.testing.DamageListener;
import net.maybemc.rushthecastle.state.phase.testing.PlacementTracker;
import net.maybemc.rushthecastle.state.phase.testing.scoreboard.TestingScoreboardHandler;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author Nico_ND1
 */
public class JumpingGamePhase extends GamePhase<RTCActor, RTCTeam, RTCGameMap, RTCGame> implements CountdownHandler {
    private final JumpingScoreboardHandler scoreboardHandler;
    private final JumpingPhase jumpingPhase;
    private final PlacementTracker placementTracker;

    public JumpingGamePhase(RTCGame game, JumpingPhase jumpingPhase) {
        super(game, jumpingPhase);
        this.jumpingPhase = jumpingPhase;

        placementTracker = new PlacementTracker();
        this.scoreboardHandler = new JumpingScoreboardHandler(game, placementTracker);
        registerListener(new JumpingScoreboardListener(game, this.scoreboardHandler));

        registerBukkitListener(GameStateEventCanceller.builder()
            .itemDrop(false)
            .itemPickup(false)
            .interactAir(false)
            .interactEntity(false)
            .interactPhysical(false)
            .interactBlock(false)
            .blockBreak(false)
            .blockPlace(false)
            .foodChange(false)
            .weather(false)
            .explosions(false)
            .build());

        registerListener(new MapObjectTickListener(game));
        registerListener(new MapObjectMoveListener(game));
        registerListener(new DamageListener(game));
        registerListener(new JumpingPhaseListener(game, jumpingPhase, placementTracker));
        registerListener(new RedstoneListener(game));

        registerItem(new ResetToCheckpointItem());
        registerItem(new PlayerHiderItem(game));
    }

    @Override
    public @Nullable GamePhase<RTCActor, RTCTeam, RTCGameMap, RTCGame> followingGamePhase() {
        return new FrozenGamePhase(game, this);
    }

    @Override
    public void handleQuit(@NotNull RTCActor actor) {
        scoreboardHandler.remove(actor);
    }

    @Override
    public void onJoin() {
        Visit currentVisit = jumpingPhase.getCurrentVisit();
        Castle castle = currentVisit.getCastle();

        for (RTCActor actor : game.getActorsPlaying()) {
            if (currentVisit.getOwner().equals(actor)) {
                actor.clearPlayer();
                actor.makeSpectatorTemporary();

                actor.setSession(null);

                actor.sendMessage("jumping-phase-started-self-visit");
                actor.sendTitle("jumping-phase-started-self-visit-title", "jumping-phase-started-self-visit-subtitle", 0, 50, 12);
            } else {
                actor.clearPlayer();

                JarSession session = castle.createSession();
                actor.setSession(session);

                for (RTCActor a : game.getActorsPlaying()) {
                    if (currentVisit.getOwner().equals(actor)) {
                        continue;
                    }

                    Player actorPlayer = a.getPlayer();
                    actorPlayer.showPlayer(game.getPlugin(), actor.getPlayer());
                    actor.getPlayer().showPlayer(game.getPlugin(), actorPlayer);
                }

                tryGiveItems(actor);

                actor.sendMessage("jumping-phase-started");
                actor.sendTitle("jumping-phase-started-title", "jumping-phase-started-subtitle", 0, 50, 12);
            }
        }

        placementTracker.reset();
        scoreboardHandler.setup();
        game.tryEnd();
    }

    @Override
    public void onQuit() {
    }

    @Override
    public @NotNull Location getSpawnPoint(@Nullable RTCActor actor) {
        if (jumpingPhase.getCurrentVisit() != null) {
            return jumpingPhase.getCurrentVisit().getCastle().getSpawnLocation();
        }
        return actor.getOwnCastle().getSpawnLocation();
    }

    @Override
    public @NotNull CountdownConfiguration getConfiguration() {
        return new CountdownConfiguration(game.getJumpingTime(), CountdownConfiguration.Direction.DOWN);
    }

    @Override
    public void onTick(int currentTick) {
        scoreboardHandler.updateTime();
    }

    @Override
    public void onStop(@NotNull StopReason reason) {
        if (reason == StopReason.FINISHED) {
            game.tryEnd();
        }
    }

    public JumpingPhase getJumpingPhase() {
        return jumpingPhase;
    }
}
