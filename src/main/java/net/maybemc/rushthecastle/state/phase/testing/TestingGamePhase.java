package net.maybemc.rushthecastle.state.phase.testing;

import net.maybemc.aurelion.game.state.countdown.CountdownConfiguration;
import net.maybemc.aurelion.game.state.countdown.CountdownHandler;
import net.maybemc.aurelion.game.state.endlogic.BiEndLogic;
import net.maybemc.aurelion.game.state.endlogic.CountdownEndLogic;
import net.maybemc.aurelion.game.state.listener.GameStateEventCanceller;
import net.maybemc.aurelion.game.state.phase.GamePhase;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.RewardConfig;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.item.ResetToCheckpointItem;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.MapObjectMoveListener;
import net.maybemc.rushthecastle.map.object.MapObjectTickListener;
import net.maybemc.rushthecastle.state.RedstoneListener;
import net.maybemc.rushthecastle.state.phase.frozen.FrozenGamePhase;
import net.maybemc.rushthecastle.state.phase.jumping.JumpingGamePhase;
import net.maybemc.rushthecastle.state.phase.jumping.JumpingPhase;
import net.maybemc.rushthecastle.state.phase.testing.scoreboard.TestingScoreboardHandler;
import net.maybemc.rushthecastle.state.phase.testing.scoreboard.TestingScoreboardListener;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author Nico_ND1
 */
public class TestingGamePhase extends GamePhase<RTCActor, RTCTeam, RTCGameMap, RTCGame> implements CountdownHandler {
    private final TestingScoreboardHandler scoreboardHandler;
    private final PlacementTracker placementTracker;

    public TestingGamePhase(RTCGame game) {
        super(game, new BiEndLogic<>(new EveryoneTestedEndLogic(), new CountdownEndLogic<>()));

        placementTracker = new PlacementTracker();
        this.scoreboardHandler = new TestingScoreboardHandler(game, placementTracker);
        registerListener(new TestingScoreboardListener(game, this.scoreboardHandler, placementTracker));

        registerBukkitListener(GameStateEventCanceller.builder()
            .itemDrop(false)
            .itemPickup(false)
            .interactAir(false)
            .interactEntity(false)
            .interactPhysical(false)
            .interactBlock(false)
            .blockBreak(false)
            .blockPlace(false)
            .foodChange(false)
            .weather(false)
            .explosions(false)
            .build());

        registerListener(new MapObjectTickListener(game));
        registerListener(new MapObjectMoveListener(game));
        registerListener(new DamageListener(game));
        registerListener(new RedstoneListener(game));

        registerItem(new ResetToCheckpointItem());
    }

    @Override
    public @Nullable GamePhase<RTCActor, RTCTeam, RTCGameMap, RTCGame> followingGamePhase() {
        JumpingGamePhase jumpingGamePhase = new JumpingGamePhase(game, new JumpingPhase(game, placementTracker));
        JumpingPhase jumpingPhase = jumpingGamePhase.getJumpingPhase();
        jumpingPhase.startVisit();

        for (RTCActor actor : game.getActorsPlaying()) {
            actor.reward(RewardConfig.Action.JUMPING_PHASE_PARTICIPATE);
            actor.incrementStat("participate-jumping");
        }
        return new FrozenGamePhase(game, jumpingGamePhase);
    }

    @Override
    public @NotNull JoinResult handleJoin(@NotNull RTCActor actor) {
        scoreboardHandler.setup(actor);
        return allowJoinAsSpectator();
    }

    @Override
    public void handleQuit(@NotNull RTCActor actor) {
        scoreboardHandler.remove(actor);
    }

    @Override
    public void onJoin() {
        if (game.tryEnd()) {
            return;
        }

        for (RTCActor actor : game.getActorsPlaying()) {
            Castle ownCastle = actor.getOwnCastle();
            ownCastle.teleport(actor);

            tryGiveItems(actor);
            actor.getPlayer().setGameMode(GameMode.ADVENTURE);

            JarSession session = ownCastle.createSession();
            actor.setSession(session);

            actor.reward(RewardConfig.Action.TESTING_PHASE_PARTICIPATE);

            actor.sendMessage("testing-phase-started");
            actor.sendTitle("testing-phase-started-title", "testing-phase-started-subtitle", 0, 40, 8);
            actor.incrementStat("participate-testing");
        }

        scoreboardHandler.setup();
    }

    @Override
    public void onQuit() {

    }

    @Override
    public @NotNull Location getSpawnPoint(@Nullable RTCActor actor) {
        if (actor == null) {
            return game.getMap().getWorld().getSpawnLocation();
        }
        return actor.getOwnCastle().getSpawnLocation();
    }

    @Override
    public @NotNull CountdownConfiguration getConfiguration() {
        return new CountdownConfiguration(game.getJumpingTime(), CountdownConfiguration.Direction.DOWN);
    }

    @Override
    public void onTick(int currentTick) {
        scoreboardHandler.updateTime();

        if (currentTick % 60 == 0 || currentTick == 30 || currentTick == 20 || currentTick <= 10) {
            if (currentTick > 60) {
                game.sendMessage("testing-phase-tick-minutes", currentTick / 60);
            } else {
                game.sendMessage("testing-phase-tick", currentTick);
            }

            if (currentTick <= 60) {
                game.getActors().forEach(actor -> actor.playSound(Sound.BLOCK_NOTE_BLOCK_BASS));
            }
        }
    }

    @Override
    public void onStop(@NotNull StopReason reason) {
        if (reason == StopReason.FINISHED) {
            game.tryEnd();
        }
    }
}
