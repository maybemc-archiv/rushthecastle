package net.maybemc.rushthecastle.state.phase.jumping.scoreboard;

import net.maybemc.aurelion.game.Games;
import net.maybemc.aurelion.game.state.GameState;
import net.maybemc.aurelion.game.state.PlayingGameState;
import net.maybemc.aurelion.game.state.countdown.Countdown;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.maybemc.meybee.spigot.scoreboard.ScoreboardLine;
import net.maybemc.meybee.spigot.scoreboard.UserScoreboard;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.theme.Theme;
import net.maybemc.rushthecastle.state.phase.testing.PlacementTracker;
import net.maybemc.rushthecastle.team.RTCTeam;

import java.util.Collection;

/**
 * @author Nico_ND1
 */
public class JumpingScoreboardHandler {
    private final RTCGame game;
    private final PlacementTracker placementTracker;
    private int actorsLineHeight;

    public JumpingScoreboardHandler(RTCGame game, PlacementTracker placementTracker) {
        this.game = game;
        this.placementTracker = placementTracker;
    }

    public void setup() {
        for (RTCActor actor : game.getActors()) {
            setup(actor);
        }
    }

    public void setup(RTCActor actor) {
        Collection<RTCActor> actorsPlaying = game.getActorsPlaying();

        actor.resetScoreboard();

        UserScoreboard scoreboard = actor.getScoreboard();
        scoreboard.setTitle(actor.translate("jumping-phase-scoreboard-title"));

        int lineHeight = actorsPlaying.size() + 7;

        scoreboard.createEmptyLines(lineHeight--);
        scoreboard.createLine("time-header", lineHeight--).setText(actor.translate("jumping-phase-scoreboard-time-header"));
        scoreboard.createLine("time-value", lineHeight--);
        scoreboard.createEmptyLines(lineHeight--);
        updateTime();

        scoreboard.createLine("theme-header", lineHeight--).setText(actor.translate("jumping-phase-scoreboard-theme-header"));
        scoreboard.createLine("theme-value", lineHeight--);
        scoreboard.createEmptyLines(lineHeight--);
        setTheme(actor, scoreboard);

        this.actorsLineHeight = lineHeight;

        for (RTCActor rtcActor : actorsPlaying) {
            Nick nick = rtcActor.getNickFor(actor);
            boolean self = rtcActor.equals(actor);
            scoreboard.createLine("actor-" + rtcActor.getId(), 0).setText(actor.translate("jumping-phase-scoreboard-player" + (self ? "-self" : ""), actor.getPoints(), nick.getDisplayName()));
        }
    }

    public void updateTime() {
        int secondsLeft = 0;

        GameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> currentState = game.getCurrentState();
        if (currentState instanceof PlayingGameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> playingGameState) {
            GameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> currentPhase = playingGameState.getCurrentPhase();
            Countdown countdown = currentPhase.getCountdown();
            secondsLeft = countdown.getRemainingTime();
        }

        String formattedString = String.format("%02d:%02d", secondsLeft / 60, secondsLeft % 60);
        for (RTCActor actor : game.getActors()) {
            actor.getScoreboard().getLine("time-value").setText(actor.translate("jumping-phase-scoreboard-time-value", formattedString));
        }
    }

    private void setTheme(RTCActor actor, UserScoreboard scoreboard) {
        Castle ownCastle = actor.getOwnCastle();
        Theme theme = ownCastle.getTheme();
        String themeName = theme.getCosmetic().translateName(actor.getUser(), game.getLanguagePack());

        scoreboard.getLine("theme-value").setText(actor.translate("jumping-phase-scoreboard-theme-value", themeName));
    }

    public void onFinish(RTCActor actor) {
        int placement = getPlacement(actor);

        for (RTCActor gameActor : game.getActors()) {
            Nick nick = actor.getNickFor(gameActor);
            UserScoreboard scoreboard = gameActor.getScoreboard();
            ScoreboardLine line = scoreboard.getLine("actor-" + actor.getId());
            boolean self = gameActor.equals(actor);
            line.setText(gameActor.translate("jumping-phase-scoreboard-player-finished" + (self ? "-self" : ""), placement, actor.getPoints(), nick.getDisplayName()));

            reorder(scoreboard);
        }
    }

    private void reorder(UserScoreboard scoreboard) {
        int lineHeight = this.actorsLineHeight;
        for (RTCActor actor : game.getTopActors()) {
            ScoreboardLine line = scoreboard.getLine("actor-" + actor.getId());
            if (line != null) {
                line.setLine(lineHeight--);
            } else {
                Games.getLogger().warning("Could not find line for actor " + actor.getId() + " in scoreboard");
            }
        }
    }

    private int getPlacement(RTCActor actor) {
        return game.getTopActors().indexOf(actor) + 1;
    }

    public void remove(RTCActor actor) {
        for (RTCActor gameActor : game.getActors()) {
            UserScoreboard scoreboard = gameActor.getScoreboard();
            ScoreboardLine line = scoreboard.getLine("actor-" + actor.getId());
            line.remove();

            reorder(scoreboard);
        }
    }
}
