package net.maybemc.rushthecastle.state.phase.building.menu;

import me.lucko.helper.item.ItemStackBuilder;
import me.lucko.helper.menu.Gui;
import net.maybemc.aurelion.game.state.item.GameStateItem;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author Nico_ND1
 */
public class BuildingMenuItem extends GameStateItem<RTCActor, RTCTeam> {
    private final BuildingMenuConfig config;

    public BuildingMenuItem(BuildingMenuConfig config) {
        super("building-menu", 8);
        this.config = config;
    }

    @Override
    public boolean shouldGiveItem(RTCActor actor) {
        Castle ownCastle = actor.getOwnCastle();
        if (ownCastle != null) {
            return !ownCastle.isFinished();
        }
        return true;
    }

    @Override
    public ItemStack getItemStack(RTCActor actor) {
        return ItemStackBuilder.of(Material.COMPARATOR)
            .name(actor.translate("item-building-menu-name"))
            .lore(actor.translate("item-building-menu-lore").split("\n"))
            .build();
    }

    @Override
    public void onInteract(RTCActor actor, PlayerInteractEvent event) {
        Gui gui = new BuildingMenuGui(actor, config);
        gui.open();
        event.setCancelled(true);
    }
}
