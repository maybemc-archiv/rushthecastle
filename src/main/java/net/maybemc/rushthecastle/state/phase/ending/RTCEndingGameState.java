package net.maybemc.rushthecastle.state.phase.ending;

import net.maybemc.aurelion.game.state.EndingGameState;
import net.maybemc.aurelion.game.state.endlogic.EndLogic;
import net.maybemc.aurelion.game.state.listener.GameStateEventCanceller;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.maybemc.meybee.spigot.scoreboard.ScoreboardLine;
import net.maybemc.meybee.spigot.scoreboard.UserScoreboard;
import net.maybemc.quests.progress.ProgressType;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.team.RTCTeam;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class RTCEndingGameState extends EndingGameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    public RTCEndingGameState(RTCGame game) {
        super(game);

        registerBukkitListener(GameStateEventCanceller.builder()
            .weather(false)
            .foodChange(false)
            .hangingBreak(false)
            .interactBlock(false)
            .interactEntity(false)
            .interactPhysical(false)
            .itemPickup(false)
            .itemDrop(false)
            .entityDamage(false)
            .build());
    }

    @Override
    public void onJoin() {
        game.assignGroupTablist();

        for (RTCActor actor : game.getActors()) {
            setupScoreboard(actor);

            actor.clearPlayer();
            tryGiveItems(actor);

            actor.getPlayer().teleport(getSpawnPoint(actor));
        }
    }

    private void setupScoreboard(RTCActor viewer) {
        viewer.resetScoreboard();

        UserScoreboard scoreboard = viewer.getScoreboard();
        scoreboard.setTitle(viewer.translate("ending-phase-scoreboard-title"));

        int lineHeight = 9;
        scoreboard.createEmptyLines(lineHeight--);

        List<RTCActor> topActors = game.getTopActors();
        for (int placement = 0; placement < 3; placement++) {
            ScoreboardLine headerLine = scoreboard.createLine("placement-" + placement + "-header", lineHeight--);
            ScoreboardLine valueLine = scoreboard.createLine("placement-" + placement + "-value", lineHeight--);

            if (placement >= topActors.size()) {
                headerLine.setText(viewer.translate("ending-phase-scoreboard-placement-header-none", placement + 1));
                valueLine.setText(viewer.translate("ending-phase-scoreboard-placement-value-none", placement + 1));
            } else {
                RTCActor actor = topActors.get(placement);
                Nick nick = actor.getNickFor(viewer);

                headerLine.setText(viewer.translate("ending-phase-scoreboard-placement-header", placement + 1, nick.getDisplayName(), actor.getPoints()));
                valueLine.setText(viewer.translate("ending-phase-scoreboard-placement-value", placement + 1, nick.getDisplayName(), actor.getPoints()));
            }

            scoreboard.createEmptyLines(lineHeight--);
        }
    }

    @Override
    public void onEnd(EndLogic.Result<RTCActor, RTCTeam> endResult) {
        RTCTeam winnerTeam = endResult.winnerTeam();

        switch (endResult.reason()) {
            case LAST_STANDING -> {
                for (RTCActor actor : game.getActors()) {
                    actor.sendMessage("game-end-LAST_STANDING", winnerTeam.getDisplayNameFor(actor));
                }
            }
            case MOST_POINTS -> {
                List<RTCActor> topActors = game.getTopActors();
                for (RTCActor actor : game.getActors()) {
                    int placement = 1;
                    for (RTCActor topActor : topActors) {
                        actor.sendMessage("game-end-MOST_POINTS-placement", placement++, topActor.getNickFor(actor).getDisplayName(), topActor.getPoints());
                    }

                    actor.sendMessage("game-end-MOST_POINTS", winnerTeam.getDisplayNameFor(actor), winnerTeam.getPoints());
                }

                winnerTeam.incrementStat("wins");
                for (RTCActor actor : winnerTeam.getActors()) {
                    actor.trackQuestProgress("wins", 1, ProgressType.INCREASE);
                }
            }
            case NONE_PLAYING -> {
                for (RTCActor actor : game.getActors()) {
                    actor.sendMessage("game-end-NONE_PLAYING");
                }
            }
            case TIMEOUT -> {
                for (RTCActor actor : game.getActors()) {
                    actor.sendMessage("game-end-TIMEOUT");
                }
            }
        }
    }
}
