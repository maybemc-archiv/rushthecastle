package net.maybemc.rushthecastle.state.phase.testing;

import net.maybemc.rushthecastle.actor.RTCActor;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Nico_ND1
 */
public class PlacementTracker {
    private int placement;
    private Map<RTCActor, Integer> knownActors = new HashMap<>();

    public int getPlacement(RTCActor actor) {
        if (knownActors.containsKey(actor)) {
            return knownActors.get(actor);
        }

        int placement = ++this.placement;
        knownActors.put(actor, placement);
        return placement;
    }

    public Optional<Integer> getPlacementLazy(RTCActor actor) {
        return Optional.ofNullable(knownActors.get(actor));
    }

    public void reset() {
        placement = 0;
        knownActors = new HashMap<>();
    }
}
