package net.maybemc.rushthecastle.state.phase.building;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.event.extent.EditSessionEvent;
import com.sk89q.worldedit.extension.platform.Actor;
import com.sk89q.worldedit.extent.NullExtent;
import com.sk89q.worldedit.util.eventbus.EventHandler;
import com.sk89q.worldedit.util.eventbus.Subscribe;
import me.lucko.helper.cooldown.Cooldown;
import me.lucko.helper.cooldown.CooldownMap;
import me.lucko.helper.scheduler.HelperExecutors;
import net.maybemc.aurelion.game.state.GameState;
import net.maybemc.aurelion.game.state.listener.BukkitActorEventHandler;
import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.aurelion.game.util.BlockFaces;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.item.ItemDataTypes;
import net.maybemc.rushthecastle.item.SchematicPlacerItemModifier;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.InsideCastleWEExtent;
import net.maybemc.rushthecastle.schematic.Schematic;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class SchematicPlacerListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private final CooldownMap<UUID> cooldowns;
    private final LoadingCache<String, Schematic> schematicCache;

    public SchematicPlacerListener(RTCGame game) {
        super(game);
        this.cooldowns = CooldownMap.create(Cooldown.ofTicks(50L));
        this.schematicCache = CacheBuilder.newBuilder()
            .expireAfterWrite(30L, TimeUnit.SECONDS)
            .build(new CacheLoader<>() {
                @Override
                public @NotNull Schematic load(@NotNull String key) throws Exception {
                    return game.getSchematicLoader().load("placer-" + key);
                }
            });
    }

    @Override
    public void register(@NotNull GameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> gameState) {
        super.register(gameState);
        WorldEdit.getInstance().getEventBus().register(this);
    }

    @Override
    public void unregister() {
        super.unregister();
        WorldEdit.getInstance().getEventBus().unregister(this);
    }

    @BukkitActorEventHandler(priority = EventPriority.HIGH, ignoreSpectator = true)
    public void onInteract(RTCActor actor, PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        Player player = event.getPlayer();
        ItemStack item = event.getItem();
        if (item == null || !item.hasItemMeta()) {
            return;
        }

        ItemMeta itemMeta = item.getItemMeta();
        PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();
        SchematicPlacerItemModifier schematicPlacer = dataContainer.get(ItemDataTypes.SCHEMATIC_PLACER_KEY, ItemDataTypes.SCHEMATIC_PLACER_TYPE);
        if (schematicPlacer == null) {
            return;
        }

        event.setCancelled(true);

        if (cooldowns.test(player.getUniqueId())) {
            Location location = event.getClickedBlock().getLocation().add(0.5D, 1, 0.5D);

            Schematic schematic = schematicCache.getUnchecked(schematicPlacer.getSchematicName());
            BlockFace facing = BlockFaces.yawToFace(player.getLocation().getYaw(), false);
            Futures.addCallback(schematic.rotateTo(facing).place(location, BukkitAdapter.adapt(player), false), result -> {
                if (!result.isOk()) {
                    actor.sendMessage("schematic-placer-failed");
                }
            }, HelperExecutors.sync());

            actor.playSound(Sound.BLOCK_COPPER_PLACE, 1, 0.275F);
            player.setCooldown(item.getType(), 50);
        } else {
            actor.sendMessage("schematic-placer-cooldown");
        }
    }

    @Subscribe
    public void onEditSession(EditSessionEvent event) {
        if (event.getStage() != EditSession.Stage.BEFORE_HISTORY) {
            return;
        }

        Actor weActor = event.getActor();
        if (weActor == null) {
            return;
        }

        RTCActor rtcActor = game.getActor(weActor.getUniqueId());
        event.setExtent(new InsideCastleWEExtent(event.getExtent(), rtcActor.getOwnCastle()));
    }
}
