package net.maybemc.rushthecastle.state.phase.testing;

import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

/**
 * @author Nico_ND1
 */
public class DamageListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private static final DamageCause[] RESETTING_DAMAGE_CAUSES = {
        DamageCause.CONTACT, DamageCause.PROJECTILE, DamageCause.FIRE, DamageCause.FIRE_TICK, DamageCause.LAVA, DamageCause.DROWNING,
        DamageCause.VOID, DamageCause.LIGHTNING, DamageCause.FALLING_BLOCK, DamageCause.DRAGON_BREATH, DamageCause.HOT_FLOOR, DamageCause.FREEZE
    };
    private static final DamageCause[] ZERO_DAMAGE_CAUSES = {
        DamageCause.POISON, DamageCause.MAGIC, DamageCause.WITHER
    };

    public DamageListener(RTCGame game) {
        super(game);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        for (DamageCause zeroDamageCause : ZERO_DAMAGE_CAUSES) {
            if (event.getCause() == zeroDamageCause) {
                event.setDamage(0);
                return;
            }
        }

        for (DamageCause resettingDamageCause : RESETTING_DAMAGE_CAUSES) {
            if (event.getCause() == resettingDamageCause) {
                tryReset(event.getEntity());
                return;
            }
        }

        event.setCancelled(true);
    }

    private void tryReset(Entity entity) {
        if (entity instanceof Player player) {
            RTCActor actor = game.getActor(player);
            actor.operateSession(jarSession -> jarSession.resetToCheckpoint(actor));
        }
    }
}
