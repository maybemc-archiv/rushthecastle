package net.maybemc.rushthecastle.state.phase.testing.scoreboard;

import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.RewardConfig;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.CastleTestedEvent;
import net.maybemc.rushthecastle.state.phase.testing.PlacementTracker;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.event.EventHandler;

/**
 * @author Nico_ND1
 */
public class TestingScoreboardListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private final TestingScoreboardHandler handler;
    private final PlacementTracker placementTracker;

    public TestingScoreboardListener(RTCGame game, TestingScoreboardHandler handler, PlacementTracker placementTracker) {
        super(game);
        this.handler = handler;
        this.placementTracker = placementTracker;
    }

    @EventHandler
    public void onCreated(CastleTestedEvent event) {
        RTCActor actor = event.getActor();
        handler.onFinish(actor);

        for (RTCActor gameActor : game.getActors()) {
            Nick nick = actor.getNickFor(gameActor);
            gameActor.sendMessage("testing-phase-finish-reached", nick.getDisplayName(), placementTracker.getPlacement(actor));
        }

        actor.reward(RewardConfig.Action.FINISH_SESSION_FOR_OWN_CASTLE);
    }
}
