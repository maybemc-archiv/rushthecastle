package net.maybemc.rushthecastle.state.phase.building.scoreboard;

import net.maybemc.aurelion.game.state.GameState;
import net.maybemc.aurelion.game.state.PlayingGameState;
import net.maybemc.aurelion.game.state.countdown.Countdown;
import net.maybemc.meybee.spigot.scoreboard.UserScoreboard;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.MapObjectType;
import net.maybemc.rushthecastle.map.theme.Theme;
import net.maybemc.rushthecastle.team.RTCTeam;

/**
 * @author Nico_ND1
 */
public class BuildingScoreboardHandler {
    private final RTCGame game;

    public BuildingScoreboardHandler(RTCGame game) {
        this.game = game;
    }

    public void setup() {
        for (RTCActor actor : game.getActorsPlaying()) {
            actor.resetScoreboard();

            UserScoreboard scoreboard = actor.getScoreboard();
            scoreboard.setTitle(actor.translate("building-phase-scoreboard-title"));

            scoreboard.createEmptyLines(1, 4, 7, 10, 13);

            createHeaderValue(scoreboard, actor, "players", 12);
            updatePlayers(actor, scoreboard);

            createHeaderValue(scoreboard, actor, "theme", 9);
            setTheme(actor, scoreboard);

            createHeaderValue(scoreboard, actor, "time", 6);

            createHeaderValue(scoreboard, actor, "finishState", 3);
            updateFinishState(actor);
        }

        updateTime();
    }

    private void createHeaderValue(UserScoreboard scoreboard, RTCActor actor, String key, int headerLine) {
        scoreboard.createLine(key + "-header", headerLine).setText(actor.translate("building-phase-scoreboard-" + key + "-header"));
        scoreboard.createLine(key + "-value", headerLine - 1);
    }

    public void updatePlayers() {
        for (RTCActor actor : game.getActorsPlaying()) {
            updatePlayers(actor, actor.getScoreboard());
        }
    }

    public void updatePlayers(RTCActor actor, UserScoreboard scoreboard) {
        int finished = (int) game.getActorsPlaying().stream()
            .filter(a -> {
                Castle ownCastle = a.getOwnCastle();
                if (ownCastle != null) {
                    return ownCastle.isFinished();
                }
                return false;
            }).count();

        scoreboard.getLine("players-value").setText(actor.translate("building-phase-scoreboard-players-value", finished, game.getActorsPlaying().size()));
    }

    private void setTheme(RTCActor actor, UserScoreboard scoreboard) {
        Castle ownCastle = actor.getOwnCastle();
        Theme theme = ownCastle.getTheme();
        String themeName = theme.getCosmetic().translateName(actor.getUser(), game.getLanguagePack());

        scoreboard.getLine("theme-value").setText(actor.translate("building-phase-scoreboard-theme-value", themeName));
    }

    public void updateTime() {
        int secondsLeft = 0;

        GameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> currentState = game.getCurrentState();
        if (currentState instanceof PlayingGameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> playingGameState) {
            GameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> currentPhase = playingGameState.getCurrentPhase();
            Countdown countdown = currentPhase.getCountdown();
            secondsLeft = countdown.getRemainingTime();
        }

        String formattedString = String.format("%02d:%02d", secondsLeft / 60, secondsLeft % 60);
        for (RTCActor actor : game.getActorsPlaying()) {
            actor.getScoreboard().getLine("time-value").setText(actor.translate("building-phase-scoreboard-time-value", formattedString));
        }
    }

    public void updateFinishState(RTCActor actor) {
        UserScoreboard scoreboard = actor.getScoreboard();
        boolean finishState = actor.getOwnCastle().findFinishObject().isPresent();

        scoreboard.getLine("finishState-value").setText(actor.translate("building-phase-scoreboard-finishState-value", actor.translate("building-phase-scoreboard-finishState-" + finishState)));
    }
}
