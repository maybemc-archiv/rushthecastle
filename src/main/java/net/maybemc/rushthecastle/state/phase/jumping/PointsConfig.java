package net.maybemc.rushthecastle.state.phase.jumping;

import net.maybemc.aurelion.game.GameConfig;

import java.util.Map;

/**
 * @author Nico_ND1
 */
public class PointsConfig extends GameConfig {
    private Map<Integer, Integer> placementToPoints;
    private Map<String, Integer> statScoreWeighting;

    public int getPoints(int placement) {
        return placementToPoints.getOrDefault(placement, 0);
    }

    public Map<String, Integer> getStatScoreWeighting() {
        return statScoreWeighting;
    }
}
