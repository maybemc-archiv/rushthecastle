package net.maybemc.rushthecastle.state.phase.testing;

import net.maybemc.aurelion.game.state.GameState;
import net.maybemc.aurelion.game.state.endlogic.EndLogic;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * @author Nico_ND1
 */
public class EveryoneTestedEndLogic implements EndLogic<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    @Override
    public @NotNull Result<RTCActor, RTCTeam> shouldEnd(@NotNull RTCGame game, @NotNull GameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> gameState) {
        Collection<RTCTeam> teamsPlaying = game.getTeamsPlaying();
        if (teamsPlaying.isEmpty()) {
            return resultOf(null, Reason.NONE_PLAYING);
        } else if (teamsPlaying.size() == 1) {
            RTCTeam winner = teamsPlaying.iterator().next();
            return resultOf(winner, Reason.LAST_STANDING);
        }

        for (RTCActor actor : game.getActorsPlaying()) {
            Castle ownCastle = actor.getOwnCastle();
            if (ownCastle == null || !ownCastle.isTested()) {
                return keepPlaying();
            }
        }
        return nextState();
    }
}
