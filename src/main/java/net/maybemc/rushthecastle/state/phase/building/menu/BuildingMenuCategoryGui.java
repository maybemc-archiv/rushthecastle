package net.maybemc.rushthecastle.state.phase.building.menu;

import me.lucko.helper.item.ItemStackBuilder;
import me.lucko.helper.menu.Gui;
import me.lucko.helper.menu.Item;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.spigot.config.GlobalDesignConfig;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.item.RTCItemStack;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

/**
 * @author Nico_ND1
 */
public class BuildingMenuCategoryGui extends Gui {
    private final RTCActor actor;
    private final BuildingMenuConfig config;
    private final BuildingMenuConfig.CategoryEntry category;

    public BuildingMenuCategoryGui(RTCActor actor, BuildingMenuConfig config, BuildingMenuConfig.CategoryEntry category) {
        super(actor.getPlayer(), 3, actor.translate("building-menu-category-" + category.getName() + "-gui-title"));
        this.actor = actor;
        this.config = config;
        this.category = category;
    }

    @Override
    public void redraw() {
        if (isFirstDraw()) {
            GlobalDesignConfig designConfig = Meybee.getInstance().getProvider(GlobalDesignConfig.class);
            designConfig.drawGlass(this, getPlayer(), 0, 2);

            setItem(18, Item.builder(designConfig.getBackItemFor(actor.getUser())).bind(ClickType.LEFT, () -> {
                Gui nextGui = new BuildingMenuGui(actor, config);
                nextGui.open();
                actor.playSound(Sound.UI_BUTTON_CLICK);
            }).build());
        }

        for (BuildingMenuConfig.ItemEntry item : config.getItems()) {
            if (!category.getName().equals(item.getCategory())) {
                continue;
            }

            RTCItemStack rtcItem = item.getItem();
            ItemStack itemStack = rtcItem.getItemStack(actor);
            setItem(item.getSlot(), ItemStackBuilder.of(itemStack).hideAttributes().build(ClickType.LEFT, () -> {
                getPlayer().getInventory().addItem(itemStack);
                actor.playSound(Sound.ENTITY_ITEM_PICKUP);
                redraw();
            }));
        }
    }
}
