package net.maybemc.rushthecastle.state.phase.building.menu;

import com.google.gson.Gson;
import lombok.Data;
import lombok.Getter;
import net.maybemc.aurelion.game.GameConfig;
import net.maybemc.meybee.api.json.GsonCreator;
import net.maybemc.meybee.api.json.GsonProvider;
import net.maybemc.rushthecastle.item.RTCGsonItemStack;
import net.maybemc.rushthecastle.item.RTCItemStack;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * @author Nico_ND1
 */
@Getter
public class BuildingMenuConfig extends GameConfig {
    private List<CategoryEntry> categories;
    private List<ItemEntry> items;

    @Data
    public static class ItemEntry {
        private int slot;
        private RTCItemStack item;
        @Nullable
        private String category;
    }

    @Data
    public static class CategoryEntry {
        private int slot;
        private String name;
        private ItemStack icon;
    }
}
