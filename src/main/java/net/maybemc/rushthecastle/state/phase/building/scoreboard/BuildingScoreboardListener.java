package net.maybemc.rushthecastle.state.phase.building.scoreboard;

import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.CastleFinishEvent;
import net.maybemc.rushthecastle.map.object.MapObject;
import net.maybemc.rushthecastle.map.object.MapObjectCreatedEvent;
import net.maybemc.rushthecastle.map.object.MapObjectDestroyedEvent;
import net.maybemc.rushthecastle.map.object.MapObjectType;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.event.EventHandler;

import java.util.Objects;

/**
 * @author Nico_ND1
 */
public class BuildingScoreboardListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private final BuildingScoreboardHandler handler;

    public BuildingScoreboardListener(RTCGame game, BuildingScoreboardHandler handler) {
        super(game);
        this.handler = handler;
    }

    @EventHandler
    public void onCreated(MapObjectCreatedEvent event) {
        handle(event.getActor(), event.getMapObject());
    }

    @EventHandler
    public void onDestroyed(MapObjectDestroyedEvent event) {
        handle(event.getActor(), event.getMapObject());
    }

    private void handle(RTCActor actor, MapObject object) {
        if (Objects.requireNonNull(object.getType()) == MapObjectType.FINISH) {
            handler.updateFinishState(actor);
        }
    }

    @EventHandler
    public void onFinish(CastleFinishEvent event) {
        handler.updatePlayers();
    }
}
