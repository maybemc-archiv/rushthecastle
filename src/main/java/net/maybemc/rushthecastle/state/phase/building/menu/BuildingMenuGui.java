package net.maybemc.rushthecastle.state.phase.building.menu;

import me.lucko.helper.item.ItemStackBuilder;
import me.lucko.helper.menu.Gui;
import me.lucko.helper.menu.Item;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.maybemc.meybee.spigot.config.ConfirmationGui;
import net.maybemc.meybee.spigot.config.GlobalDesignConfig;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.item.RTCItemStack;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

/**
 * @author Nico_ND1
 */
public class BuildingMenuGui extends Gui {
    private final RTCActor actor;
    private final BuildingMenuConfig config;

    public BuildingMenuGui(RTCActor actor, BuildingMenuConfig config) {
        super(actor.getPlayer(), 4, actor.translate("building-menu-gui-title"));
        this.actor = actor;
        this.config = config;
    }

    @Override
    public void redraw() {
        if (isFirstDraw()) {
            GlobalDesignConfig designConfig = Meybee.getInstance().getProvider(GlobalDesignConfig.class);
            designConfig.drawGlass(this, getPlayer(), 0, 3);
        }

        for (BuildingMenuConfig.CategoryEntry category : config.getCategories()) {
            setItem(category.getSlot(), ItemStackBuilder.of(category.getIcon())
                .hideAttributes()
                .name(actor.translate("building-menu-category-" + category.getName() + "-name"))
                .lore(actor.translate("building-menu-category-" + category.getName() + "-lore").split("\n"))
                .build(ClickType.LEFT, () -> {
                    Gui nextGui = new BuildingMenuCategoryGui(actor, config, category);
                    nextGui.open();
                    actor.playSound(Sound.UI_BUTTON_CLICK);
                }));
        }

        for (BuildingMenuConfig.ItemEntry item : config.getItems()) {
            if (item.getCategory() != null) {
                continue;
            }

            RTCItemStack rtcItem = item.getItem();
            ItemStack itemStack = rtcItem.getItemStack(actor);
            setItem(item.getSlot(), ItemStackBuilder.of(itemStack).hideAttributes().build(ClickType.LEFT, () -> {
                getPlayer().getInventory().addItem(itemStack);
                actor.playSound(Sound.ENTITY_ITEM_PICKUP);
                redraw();
            }));
        }

        setItem(31, ItemStackBuilder.of(Material.PLAYER_HEAD)
            .texture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTkyZTMxZmZiNTljOTBhYjA4ZmM5ZGMxZmUyNjgwMjAzNWEzYTQ3YzQyZmVlNjM0MjNiY2RiNDI2MmVjYjliNiJ9fX0=")
            .name(actor.translate("building-menu-finish-name"))
            .lore(actor.translate("building-menu-finish-lore").split("\n"))
            .build(ClickType.LEFT, () -> {
                if (actor.getOwnCastle().findFinishObject().isEmpty()) {
                    actor.sendMessage("building-menu-finish-no-finish");
                    return;
                }

                RTCGame game = Meybee.getInstance().getProvider(RTCGame.class);
                Gui nextGui = new ConfirmationGui(getPlayer(), game.getLanguagePack(), "building-menu-finish", this) {
                    @Override
                    public void confirm() {
                        actor.finishBuilding();
                        close();

                        for (RTCActor rtcActor : game.getActorsPlaying()) {
                            Nick nick = actor.getNickFor(rtcActor);
                            rtcActor.sendMessage("building-finished", nick.getDisplayName());
                            rtcActor.playSound(Sound.BLOCK_NOTE_BLOCK_BASS);
                        }

                        game.tryEnd();
                    }
                };
                nextGui.open();
                actor.playSound(Sound.UI_BUTTON_CLICK);
            }));
    }
}
