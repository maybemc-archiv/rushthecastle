package net.maybemc.rushthecastle.state.phase.jumping.scoreboard;

import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.CastleFinishEvent;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.event.EventHandler;

/**
 * @author Nico_ND1
 */
public class JumpingScoreboardListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private final JumpingScoreboardHandler handler;

    public JumpingScoreboardListener(RTCGame game, JumpingScoreboardHandler handler) {
        super(game);
        this.handler = handler;
    }

    @EventHandler
    public void onCreated(CastleFinishEvent event) {
        handler.onFinish(event.getActor());
    }
}
