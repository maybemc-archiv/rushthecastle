package net.maybemc.rushthecastle.state.phase.jumping;

import net.maybemc.aurelion.game.state.GameState;
import net.maybemc.aurelion.game.state.endlogic.CountdownEndLogic;
import net.maybemc.aurelion.game.state.endlogic.EndLogic;
import net.maybemc.aurelion.game.state.endlogic.MostPointsEndLogic;
import net.maybemc.aurelion.game.state.endlogic.TimeoutEndLogic;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.state.phase.testing.PlacementTracker;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Nico_ND1
 */
public class JumpingPhase implements EndLogic<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private final RTCGame game;
    private final PlacementTracker placementTracker;
    private final EndLogic<RTCActor, RTCTeam, RTCGameMap, RTCGame> mostPointsEndLogic;
    private final EndLogic<RTCActor, RTCTeam, RTCGameMap, RTCGame> countdownEndLogic;
    private final List<Visit> castlesToVisit;
    private Visit currentVisit;

    public JumpingPhase(RTCGame game, PlacementTracker placementTracker) {
        this.game = game;
        this.placementTracker = placementTracker;

        this.mostPointsEndLogic = new MostPointsEndLogic<>(0, RTCTeam::getPoints);
        this.countdownEndLogic = new TimeoutEndLogic<>();

        Collection<RTCActor> actorsPlaying = game.getActorsPlaying();
        this.castlesToVisit = new ArrayList<>(actorsPlaying.size());
        for (RTCActor actor : actorsPlaying) {
            castlesToVisit.add(new Visit(actor, actor.getOwnCastle()));
        }
    }

    public boolean isHiddenInScoreboard(RTCActor actor) {
        if (currentVisit != null) {
            return currentVisit.getOwner().equals(actor);
        }
        return false;
    }

    public boolean isFinished() {
        return castlesToVisit.isEmpty() && currentVisit == null;
    }

    @Override
    public @NotNull Result<RTCActor, RTCTeam> shouldEnd(@NotNull RTCGame rtcGame, @NotNull GameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> gameState) {
        Collection<RTCTeam> teamsPlaying = game.getTeamsPlaying();
        if (teamsPlaying.isEmpty()) {
            return resultOf(null, Reason.NONE_PLAYING);
        } else if (teamsPlaying.size() == 1) {
            RTCTeam winner = teamsPlaying.iterator().next();
            return resultOf(winner, Reason.LAST_STANDING);
        }

        Result<RTCActor, RTCTeam> countdownResult = countdownEndLogic.shouldEnd(rtcGame, gameState);
        if (countdownResult.reason().isEndsGame()) {
            return countdownResult;
        }

        if (isFinished()) {
            return mostPointsEndLogic.shouldEnd(rtcGame, gameState);
        }

        if (currentVisit == null && !castlesToVisit.isEmpty()) {
            return nextState();
        }
        return keepPlaying();
    }

    public void startVisit() {
        if (castlesToVisit.isEmpty()) {
            throw new IllegalStateException("No more castles to visit");
        }

        currentVisit = castlesToVisit.remove(0);
        placementTracker.reset();

        Castle castle = currentVisit.getCastle();
        for (RTCActor actor : game.getActorsPlaying()) {
            castle.teleport(actor);
            actor.clearPlayer();

            Nick nick = currentVisit.getOwner().getNickFor(actor);
            actor.sendMessage("visit-start", nick.getDisplayName());
        }
    }

    public void nextVisit() {
        currentVisit = null;
        game.tryEnd();

        if (!isFinished()) {
            startVisit();
        }
    }

    public boolean isCurrentVisitFinished() {
        for (RTCActor actor : game.getActorsPlaying()) {
            if (actor.equals(currentVisit.getOwner())) {
                continue;
            }

            JarSession session = actor.getSession().orElseThrow(IllegalStateException::new);

            if (!session.isFinished()) {
                return false;
            }
        }
        return true;
    }

    public Visit getCurrentVisit() {
        return currentVisit;
    }
}
