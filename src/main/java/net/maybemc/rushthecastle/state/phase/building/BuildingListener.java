package net.maybemc.rushthecastle.state.phase.building;

import net.maybemc.aurelion.game.state.listener.BukkitActorEventHandler;
import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author Nico_ND1
 */
public class BuildingListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    public BuildingListener(RTCGame game) {
        super(game);
    }

    @BukkitActorEventHandler(ignoreSpectator = true, ignoreCancelled = true)
    public void onBlockPlace(RTCActor actor, BlockPlaceEvent event) {
        Castle castle = actor.getOwnCastle();
        ItemStack itemInHand = event.getItemInHand();
        if (itemInHand.getType() == Material.BARRIER) {
            event.setCancelled(true);
            return;
        }

        if (!castle.canBeBuiltAt(event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    @BukkitActorEventHandler(ignoreSpectator = true, ignoreCancelled = true)
    public void onBlockBreak(RTCActor actor, BlockBreakEvent event) {
        Castle castle = actor.getOwnCastle();
        if (!castle.canBeBuiltAt(event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        if (event.getPlayer().getInventory().getHeldItemSlot() == 8) {
            event.setCancelled(true);
            return;
        }

        event.getItemDrop().remove();
    }

    @EventHandler
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        event.setCancelled(true);
    }
}
