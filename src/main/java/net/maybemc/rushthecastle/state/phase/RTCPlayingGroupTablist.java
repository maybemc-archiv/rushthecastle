package net.maybemc.rushthecastle.state.phase;

import net.maybemc.meybee.spigot.tablist.GroupTablist;
import net.maybemc.meybee.spigot.tablist.TablistContext;
import org.bukkit.scoreboard.Team;

/**
 * @author Nico_ND1
 */
public class RTCPlayingGroupTablist extends GroupTablist {
    @Override
    public Team applyTablist(TablistContext context) {
        Team team = super.applyTablist(context);
        if (team != null) {
            team.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
        }
        return team;
    }
}
