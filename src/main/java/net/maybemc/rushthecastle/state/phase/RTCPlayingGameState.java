package net.maybemc.rushthecastle.state.phase;

import net.maybemc.aurelion.game.state.PlayingGameState;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.spigot.tablist.GroupTablist;
import net.maybemc.meybee.spigot.tablist.TablistRepository;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.state.phase.ending.RTCEndingGameState;
import net.maybemc.rushthecastle.team.RTCTeam;

/**
 * @author Nico_ND1
 */
public class RTCPlayingGameState extends PlayingGameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    public RTCPlayingGameState(RTCGame game) {
        super(game, new RTCEndingGameState(game));
    }

    @Override
    public void assignTeamTablist() {
        game.assignGroupTablist();
    }

    @Override
    protected void assignTeams() {
        for (RTCActor actor : game.getActors()) {
            if (actor.getPlayState().isWaiting() || actor.getPlayState().isPlaying()) {
                RTCTeam team = actor.getTeam();
                if (team == null) {
                    team = findTeam();
                    team.addActor(actor);
                }
            }
        }
    }
}
