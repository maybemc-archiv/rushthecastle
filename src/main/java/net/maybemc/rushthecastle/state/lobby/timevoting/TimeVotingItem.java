package net.maybemc.rushthecastle.state.lobby.timevoting;

import me.lucko.helper.item.ItemStackBuilder;
import me.lucko.helper.menu.Gui;
import net.maybemc.aurelion.game.state.item.GameStateItem;
import net.maybemc.meybee.api.i18n.DurationTranslator;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author Nico_ND1
 */
public class TimeVotingItem extends GameStateItem<RTCActor, RTCTeam> {
    private final TimeVoting timeVoting;
    private final DurationTranslator durationTranslator;

    public TimeVotingItem(TimeVoting timeVoting, DurationTranslator durationTranslator) {
        super("time-voting", 4);
        this.timeVoting = timeVoting;
        this.durationTranslator = durationTranslator;
    }

    @Override
    public ItemStack getItemStack(RTCActor actor) {
        return ItemStackBuilder.of(Material.CLOCK)
            .name(actor.translate("time-voting-item-name"))
            .lore(actor.translate("time-voting-item-lore").split("\n"))
            .build();
    }

    @Override
    public void onInteract(RTCActor actor, PlayerInteractEvent event) {
        if (timeVoting.isDisabled()) {
            actor.sendMessage("time-voting-disabled");
            return;
        }

        Gui nextGui = new TimeVotingGui(actor, timeVoting, durationTranslator);
        nextGui.open();

        event.setUseItemInHand(Event.Result.DENY);
        actor.playSound(Sound.UI_BUTTON_CLICK);
    }
}
