package net.maybemc.rushthecastle.state.lobby;

import net.maybemc.aurelion.game.map.LobbyGameMap;
import net.maybemc.aurelion.maps.Map;
import net.maybemc.meybee.spigot.hologram.packet.DistanceHologram;
import net.maybemc.meybee.spigot.hologram.packet.DistanceHologramLine;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.GameRule;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.type.Gate;
import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

/**
 * @author Nico_ND1
 */
public class RTCLobbyGameMap extends LobbyGameMap<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private final LobbyMapConfig config;
    private int tickSkip;

    public RTCLobbyGameMap(@NotNull Map map, @NotNull RTCGame game) {
        super(map, game);
        this.config = loadConfig(LobbyMapConfig.class);
    }

    @Override
    public void setWorld(World world) {
        super.setWorld(world);

        updateClock();
        spawnUpdateHologram();
    }

    private void spawnUpdateHologram() {
        Location spawn = this.config.getUpdateHologramSpawn().toLocation(getWorld());
        int lines = this.config.getUpdateHologramLines();

        DistanceHologram hologram = new DistanceHologram(spawn, 60.0D);
        for (int i = 0; i < lines; i++) {
            int line = i;
            hologram.addLine(DistanceHologramLine.create(i, user -> {
                return game.getActor(user.getUniqueId()).translate("lobby-update-hologram-line-" + line);
            }));
        }
        hologram.spawn();
    }

    public void updateClock() {
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        if (hour >= 12) {
            hour -= 12;
        }

        LobbyMapConfig.ClockPosition actualPosition = null;
        for (LobbyMapConfig.ClockPosition clockPosition : config.getClockPositions()) {
            if (hour >= clockPosition.getMinHour()) {
                if (actualPosition == null || clockPosition.getMinHour() > actualPosition.getMinHour()) {
                    actualPosition = clockPosition;
                }
            }
        }

        for (LobbyMapConfig.ClockPosition clockPosition : config.getClockPositions()) {
            Location location = clockPosition.getVector().toLocation(getWorld());
            Block block = location.getBlock();
            if (clockPosition.equals(actualPosition)) {
                block.setType(clockPosition.getBlockType());
                if (clockPosition.getFenceDirection() != null) {
                    Gate gate = (Gate) block.getBlockData();
                    gate.setFacing(clockPosition.getFenceDirection());
                    block.setBlockData(gate);
                }
            } else {
                block.setType(Material.AIR);
            }
        }
    }

    public void tick() {
        if (tickSkip++ == 1200) {
            updateClock();
        }
    }

    public LobbyMapConfig getConfig() {
        return config;
    }
}
