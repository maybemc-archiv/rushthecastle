package net.maybemc.rushthecastle.state.lobby.timevoting;

import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Nico_ND1
 */
public class TimeVotingListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    public TimeVotingListener(RTCGame game) {
        super(game);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        TimeVotingGui.update();
    }
}
