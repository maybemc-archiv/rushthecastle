package net.maybemc.rushthecastle.state.lobby;

import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.aurelion.game.state.lobby.LobbyScoreboardListener;
import net.maybemc.aurelion.game.state.lobby.LobbyScoreboardListener.Component;
import net.maybemc.cosmetics.event.CosmeticSelectedEvent;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.event.EventHandler;

/**
 * @author Nico_ND1
 */
public class ThemeSelectListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private final Component themeComponent;
    private final LobbyScoreboardListener<RTCActor, RTCTeam, RTCGameMap, RTCGame> scoreboardListener;

    public ThemeSelectListener(RTCGame game, Component themeComponent, LobbyScoreboardListener<RTCActor, RTCTeam, RTCGameMap, RTCGame> scoreboardListener) {
        super(game);
        this.themeComponent = themeComponent;
        this.scoreboardListener = scoreboardListener;
    }

    @EventHandler
    public void onCosmeticSelect(CosmeticSelectedEvent event) {
        scoreboardListener.update(this.themeComponent);
    }
}
