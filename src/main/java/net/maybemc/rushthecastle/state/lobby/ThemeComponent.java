package net.maybemc.rushthecastle.state.lobby;

import me.lucko.helper.scheduler.HelperExecutors;
import net.maybemc.aurelion.game.Game;
import net.maybemc.aurelion.game.actor.Actor;
import net.maybemc.aurelion.game.state.lobby.LobbyScoreboardListener;
import net.maybemc.aurelion.game.state.lobby.LobbyScoreboardListener.Component;
import net.maybemc.cosmetics.CosmeticInventory;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.spigot.scoreboard.ScoreboardLine;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.team.RTCTeam;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class ThemeComponent implements Component {
    private final LobbyScoreboardListener<RTCActor, RTCTeam, RTCGameMap, RTCGame> scoreboardListener;

    public ThemeComponent(LobbyScoreboardListener<RTCActor, RTCTeam, RTCGameMap, RTCGame> scoreboardListener) {
        this.scoreboardListener = scoreboardListener;
    }

    @Override
    public void update(Actor<?, ?> actor, Game<?, ?, ?, ?> game, ScoreboardLine scoreboardLine) {
        CompletableFuture<CosmeticInventory> cosmeticInventory = actor.getCosmeticInventory();
        if (cosmeticInventory.isDone()) {
            CosmeticInventory inventory = cosmeticInventory.getNow(null);
            boolean randomTheme = inventory.hasRandomCosmeticSelected("rtc-theme");
            if (randomTheme) {
                scoreboardLine.setText(actor.translate(getValueKey(), actor.translate("rtc-theme-random")));
            } else {
                String theme = inventory.getSelectedCosmetic("rtc-theme").map(cosmetic -> cosmetic.translateName(actor.getUser(), game.getLanguagePack())).orElse("?");
                scoreboardLine.setText(actor.translate(getValueKey(), theme));
            }
        } else {
            Futures.addCallback(cosmeticInventory, inventory -> {
                if (game.getCurrentState() instanceof RTCLobbyGameState && actor.isOnline()) {
                    scoreboardListener.update(this, (RTCActor) actor);
                }
            }, HelperExecutors.sync());
        }
    }

    @Override
    public String name() {
        return "theme";
    }
}
