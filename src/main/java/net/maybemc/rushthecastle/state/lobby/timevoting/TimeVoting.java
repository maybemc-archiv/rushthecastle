package net.maybemc.rushthecastle.state.lobby.timevoting;

import lombok.Data;
import lombok.Getter;
import me.lucko.helper.menu.Gui;
import me.lucko.helper.scheduler.Ticks;
import net.maybemc.aurelion.game.property.PropertyKey;
import net.maybemc.meybee.api.i18n.DurationTranslator;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Nico_ND1
 */
// TODO: implement vote power
public class TimeVoting {
    private final RTCGame game;
    private final List<VoteHolder> buildingPhaseVotes;
    private final List<VoteHolder> jumpingPhaseVotes;
    private final DurationTranslator durationTranslator;
    private boolean disabled;

    public TimeVoting(TimeVotingConfig config, RTCGame game, DurationTranslator durationTranslator) {
        this.game = game;
        this.durationTranslator = durationTranslator;

        this.buildingPhaseVotes = new ArrayList<>();
        for (Integer buildingPhaseTime : config.getBuildingPhaseTimes()) {
            VoteHolder voteHolder = new VoteHolder(VoteType.BUILDING, buildingPhaseTime);
            this.buildingPhaseVotes.add(voteHolder);
        }

        this.jumpingPhaseVotes = new ArrayList<>();
        for (Integer jumpingPhaseTime : config.getJumpingPhaseTimes()) {
            VoteHolder voteHolder = new VoteHolder(VoteType.JUMPING, jumpingPhaseTime);
            this.jumpingPhaseVotes.add(voteHolder);
        }
    }

    public void finish() {
        disable();

        VoteHolder buildingVoteWinner = getBuildingVoteWinner();
        game.setBuildingTime(buildingVoteWinner.getTime());

        VoteHolder jumpingVoteWinner = getJumpingVoteWinner();
        game.setJumpingTime(jumpingVoteWinner.getTime());

        for (RTCActor actor : game.getActors()) {
            actor.sendMessage(
                "time-voting-finished",
                durationTranslator.translate(actor.getUniqueId(), TimeUnit.SECONDS.toMillis(buildingVoteWinner.getTime())),
                buildingVoteWinner.getVotes(),
                durationTranslator.translate(actor.getUniqueId(), TimeUnit.SECONDS.toMillis(jumpingVoteWinner.getTime())),
                jumpingVoteWinner.getVotes()
            );
        }
    }

    private VoteHolder getBuildingVoteWinner() {
        return buildingPhaseVotes.stream()
            .filter(voteHolder -> voteHolder.getVotes() > 0)
            .max(Comparator.comparingInt(VoteHolder::getVotes))
            .orElse(new VoteHolder(VoteType.BUILDING, 3 * 60));
    }

    private VoteHolder getJumpingVoteWinner() {
        return jumpingPhaseVotes.stream()
            .filter(voteHolder -> voteHolder.getVotes() > 0)
            .max(Comparator.comparingInt(VoteHolder::getVotes))
            .orElse(new VoteHolder(VoteType.JUMPING, 3 * 60));
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void disable() {
        this.disabled = true;
        for (Gui openGui : Gui.getOpenGuis(TimeVotingGui.class)) {
            openGui.close();
        }
    }

    public List<VoteHolder> getBuildingPhaseVotes() {
        return buildingPhaseVotes;
    }

    public List<VoteHolder> getJumpingPhaseVotes() {
        return jumpingPhaseVotes;
    }

    @Data
    public class VoteHolder {
        private static final AtomicInteger ID_COUNT = new AtomicInteger();

        private final int id = ID_COUNT.incrementAndGet();
        private final VoteType type;
        private final int time;

        public int getVotes() {
            return (int) game.getActors().stream()
                .filter(actor -> actor.getProperty(type.getVoteKey(), 0) == id)
                .count();
        }

        public boolean addVote(RTCActor actor) {
            return actor.findProperty(type.getVoteKey()).map(vote -> {
                if (vote != id) {
                    actor.setProperty(type.getVoteKey(), id);
                    return true;
                }
                return false;
            }).orElseGet(() -> {
                actor.setProperty(type.getVoteKey(), id);
                return true;
            });
        }

        public boolean hasVoted(RTCActor actor) {
            return actor.getProperty(type.getVoteKey(), 0) == id;
        }
    }

    @Getter
    public enum VoteType {
        BUILDING,
        JUMPING;

        private final PropertyKey<Integer> voteKey;

        VoteType() {
            this.voteKey = PropertyKey.create("vote-type-" + name(), Integer.class);
        }
    }
}
