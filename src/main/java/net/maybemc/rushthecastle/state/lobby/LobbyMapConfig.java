package net.maybemc.rushthecastle.state.lobby;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.maybemc.aurelion.game.map.GameMapConfig;
import net.maybemc.aurelion.game.state.lobby.stats.RankWallConfig;
import net.maybemc.meybee.spigot.config.DirectedVector;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

import java.util.List;

/**
 * @author Nico_ND1
 */
@Getter
public class LobbyMapConfig extends GameMapConfig {
    private DirectedVector spawn;
    private List<ClockPosition> clockPositions;
    private Vector statsHologram;
    private List<RankWallConfig> rankWalls;
    private Vector updateHologramSpawn;
    private int updateHologramLines;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ClockPosition {
        private Vector vector;
        private Material blockType;
        private BlockFace fenceDirection;
        private int minHour;
    }
}
