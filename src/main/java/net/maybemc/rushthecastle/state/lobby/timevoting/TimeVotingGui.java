package net.maybemc.rushthecastle.state.lobby.timevoting;

import me.lucko.helper.item.ItemStackBuilder;
import me.lucko.helper.menu.Gui;
import me.lucko.helper.scheduler.Ticks;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.i18n.DurationTranslator;
import net.maybemc.meybee.spigot.config.GlobalDesignConfig;
import net.maybemc.rushthecastle.actor.RTCActor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.inventory.ClickType;

import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class TimeVotingGui extends Gui {
    private static final int[] BUILDING_VOTE_SLOTS = {10, 11, 12, 10 + 9, 11 + 9, 12 + 9};
    private static final int[] JUMPING_VOTE_SLOTS = {14, 15, 16, 14 + 9, 15 + 9, 16 + 9};

    private final RTCActor actor;
    private final TimeVoting timeVoting;
    private final DurationTranslator durationTranslator;

    public TimeVotingGui(RTCActor actor, TimeVoting timeVoting, DurationTranslator durationTranslator) {
        super(actor.getPlayer(), 4, actor.translate("time-voting-gui-title"));
        this.actor = actor;
        this.timeVoting = timeVoting;
        this.durationTranslator = durationTranslator;
    }

    @Override
    public void redraw() {
        if (isFirstDraw()) {
            GlobalDesignConfig designConfig = Meybee.getInstance().getProvider(GlobalDesignConfig.class);
            designConfig.drawGlass(this, actor.getUser(), 0, 3);
        }

        int i = 0;
        for (TimeVoting.VoteHolder buildingPhaseVote : timeVoting.getBuildingPhaseVotes()) {
            int slot = BUILDING_VOTE_SLOTS[i++];
            drawVoteHolder(slot, Material.COMPASS, buildingPhaseVote);
        }

        i = 0;
        for (TimeVoting.VoteHolder jumpingPhaseVote : timeVoting.getJumpingPhaseVotes()) {
            int slot = JUMPING_VOTE_SLOTS[i++];
            drawVoteHolder(slot, Material.RECOVERY_COMPASS, jumpingPhaseVote);
        }
    }

    private void drawVoteHolder(int slot, Material icon, TimeVoting.VoteHolder voteHolder) {
        String duration = durationTranslator.translate(actor.getUniqueId(), TimeUnit.SECONDS.toMillis(voteHolder.getTime()));
        int votes = voteHolder.getVotes();
        TimeVoting.VoteType type = voteHolder.getType();
        setItem(slot, ItemStackBuilder.of(icon)
            .conditionalGlow(() -> voteHolder.hasVoted(actor))
            .name(actor.translate("time-voting-gui-" + type.name() + "-vote-name", duration, votes))
            .lore(actor.translate("time-voting-gui-" + type.name() + "-vote-lore", duration, votes).split("\n"))
            .build(ClickType.LEFT, () -> {
                if (voteHolder.addVote(actor)) {
                    actor.playSound(Sound.BLOCK_AMETHYST_CLUSTER_PLACE);
                    update();
                }
            })
        );
    }

    public static void update() {
        for (TimeVotingGui openGui : Gui.getOpenGuis(TimeVotingGui.class)) {
            openGui.redraw();
        }
    }
}
