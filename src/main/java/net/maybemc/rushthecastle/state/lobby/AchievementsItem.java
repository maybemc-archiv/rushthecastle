package net.maybemc.rushthecastle.state.lobby;

import me.lucko.helper.item.ItemStackBuilder;
import net.maybemc.aurelion.game.state.item.GameStateItem;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author Nico_ND1
 */
public class AchievementsItem extends GameStateItem<RTCActor, RTCTeam> {
    public AchievementsItem() {
        super("achievements", 7);
    }

    @Override
    public ItemStack getItemStack(RTCActor actor) {
        return ItemStackBuilder.of(Material.NETHER_STAR)
            .name(actor.translate("achievements-item-name"))
            .build();
    }

    @Override
    public void onInteract(RTCActor actor, PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            actor.playSound(Sound.UI_BUTTON_CLICK);
            actor.getPlayer().performCommand("openquests dominik");
        }
    }
}
