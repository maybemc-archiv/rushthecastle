package net.maybemc.rushthecastle.state.lobby.timevoting;

import lombok.Getter;
import net.maybemc.aurelion.game.GameConfig;

import java.util.List;

/**
 * @author Nico_ND1
 */
@Getter
public class TimeVotingConfig extends GameConfig {
    private List<Integer> buildingPhaseTimes;
    private List<Integer> jumpingPhaseTimes;
}
