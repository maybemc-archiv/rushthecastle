package net.maybemc.rushthecastle.state.lobby;

import net.maybemc.aurelion.game.state.GameState;
import net.maybemc.aurelion.game.state.item.BackToLobbyItem;
import net.maybemc.aurelion.game.state.listener.GameStateEventCanceller;
import net.maybemc.aurelion.game.state.listener.GameStateListenerTask;
import net.maybemc.aurelion.game.state.lobby.IngameCosmeticsItem;
import net.maybemc.aurelion.game.state.lobby.LobbyGameState;
import net.maybemc.aurelion.game.state.lobby.LobbyScoreboardListener;
import net.maybemc.aurelion.game.state.lobby.LobbyScoreboardListener.Component;
import net.maybemc.aurelion.game.state.lobby.LobbyScoreboardListener.Components;
import net.maybemc.meybee.api.i18n.DurationTranslator;
import net.maybemc.meybee.api.i18n.DurationTranslatorBuilder;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.state.lobby.timevoting.TimeVoting;
import net.maybemc.rushthecastle.state.lobby.timevoting.TimeVotingConfig;
import net.maybemc.rushthecastle.state.lobby.timevoting.TimeVotingItem;
import net.maybemc.rushthecastle.state.lobby.timevoting.TimeVotingListener;
import net.maybemc.rushthecastle.state.phase.RTCPlayingGameState;
import net.maybemc.rushthecastle.state.phase.building.BuildingGamePhase;
import net.maybemc.rushthecastle.state.phase.frozen.FrozenGamePhase;
import net.maybemc.rushthecastle.state.phase.testing.TestingGamePhase;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class RTCLobbyGameState extends LobbyGameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private TimeVoting timeVoting;

    public RTCLobbyGameState(@NotNull RTCGame game) {
        super(game);

        LobbyScoreboardListener<RTCActor, RTCTeam, RTCGameMap, RTCGame> scoreboardListener = enableDefaultScoreboard();
        Component themeComponent = new ThemeComponent(scoreboardListener);
        scoreboardListener.components(Components.GAME_NAME, themeComponent, Components.GAME_ACTORS);

        registerListener(new ThemeSelectListener(game, themeComponent, scoreboardListener));

        registerItem(new IngameCosmeticsItem<>(new ItemStack(Material.ORANGE_SHULKER_BOX), 0, "rushthecastle"));
        registerItem(new AchievementsItem());
        registerItem(new BackToLobbyItem<>(false));

        registerBukkitListener(GameStateEventCanceller.builder()
            .itemDrop(false)
            .itemPickup(false)
            .interactAir(false)
            .interactEntity(false)
            .interactPhysical(false)
            .interactBlock(false)
            .hangingBreak(false)
            .foodChange(false)
            .weather(false)
            .explosions(false)
            .entityDamage(false)
            .armorStandManipulate(false)
            .build());

        RTCLobbyGameMap lobbyMap = (RTCLobbyGameMap) game.getLobbyMap();
        LobbyMapConfig lobbyMapConfig = lobbyMap.getConfig();
        registerBukkitListener(new LobbyListener(lobbyMapConfig.getSpawn().toLocation(lobbyMap.getWorld())));
        spawnStatsHologram("rushthecastle", lobbyMapConfig.getStatsHologram());
        lobbyMapConfig.getRankWalls().forEach(config -> enableRankWall("rushthecastle", config));

        registerListener(new GameStateListenerTask<>(1, 1) {
            @Override
            public void run() {
                ((RTCLobbyGameMap) game.getLobbyMap()).tick();
            }
        });

        enableTimeVoting();
    }

    private void enableTimeVoting() {
        DurationTranslator durationTranslator = DurationTranslatorBuilder.create()
            .mode(DurationTranslator.Mode.ALL_TIME_UNITS)
            .timeUnits(TimeUnit.MINUTES)
            .build();

        TimeVotingConfig config = game.getTimeVotingConfig();
        timeVoting = new TimeVoting(config, game, durationTranslator);

        registerItem(new TimeVotingItem(timeVoting, durationTranslator));
        registerListener(new TimeVotingListener(game));
    }

    @Override
    public void onTick(int currentTick) {
        super.onTick(currentTick);

        if (currentTick == 10) {
            timeVoting.finish();
        } else if (currentTick == 7) {
            for (RTCActor actor : game.getActorsPlaying()) {
                actor.loadOwnCastle(game);
            }
        }
    }

    @Override
    public void onQuit() {
        super.onQuit();

        for (RTCActor actor : game.getActorsPlaying()) {
            actor.loadOwnCastle(game);
            actor.incrementStat("games");
        }
    }

    @Override
    public @Nullable GameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> followingGameState() {
        return new RTCPlayingGameState(game)
            .registerPhase(new FrozenGamePhase(game, new BuildingGamePhase(game)))
            .registerPhase(new FrozenGamePhase(game, new TestingGamePhase(game))); // TestingGamePhase will continue with JumpingGamePhase
    }
}
