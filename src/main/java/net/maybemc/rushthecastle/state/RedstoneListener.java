package net.maybemc.rushthecastle.state;

import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockRedstoneEvent;

/**
 * @author Nico_ND1
 */
public class RedstoneListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    public RedstoneListener(RTCGame game) {
        super(game);
    }

    @EventHandler
    public void onRedstone(BlockRedstoneEvent event) {
        event.setNewCurrent(0);
    }
}
