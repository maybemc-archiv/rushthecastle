package net.maybemc.rushthecastle;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.json.Config;
import net.maybemc.meybee.api.json.GsonCreator;
import net.maybemc.meybee.api.json.GsonProvider;
import net.maybemc.shop.config.GsonIconSupplier;
import net.maybemc.shop.config.ShopConfig;
import net.maybemc.shop.icon.IconSupplier;
import net.maybemc.shop.product.GsonProduct;
import net.maybemc.shop.product.Product;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * @author Nico_ND1
 */
@Config(filePath = "*/game-rewards")
public class RewardConfig implements GsonCreator {
    private Map<String, Product> actionToReward;

    public Product getReward(Action action, String arg) {
        if (!action.hasArg) {
            throw new IllegalStateException("Reward action " + action.name() + " has no argument: " + arg);
        }
        return actionToReward.get(action.name() + "-" + arg);
    }

    public Product getReward(Action action) {
        return actionToReward.get(action.name());
    }

    @Override
    public @NotNull Gson createGson(@NotNull GsonProvider gsonProvider) {
        ShopConfig shopConfig = Meybee.getInstance().getProvider(ShopConfig.class);
        return gsonProvider.createBuilder()
            .registerTypeAdapter(Product.class, new GsonProduct(shopConfig.getProductFactories()))
            .registerTypeAdapter(IconSupplier.class, new GsonIconSupplier())
            .create();
    }

    @AllArgsConstructor
    public enum Action {
        FINISH_SESSION_FOR_OWN_CASTLE(false),
        FINISH_SESSION_FOR_VISITED_CASTLE(false),
        SESSION_PLACEMENT(true),
        BUILDING_PHASE_PARTICIPATE(false),
        TESTING_PHASE_PARTICIPATE(false),
        JUMPING_PHASE_PARTICIPATE(false);

        private final boolean hasArg;
    }
}
