package net.maybemc.rushthecastle.map.object.attributes;

/**
 * @author Nico_ND1
 */
public interface Ticking {
    void tick(int tick);
}
