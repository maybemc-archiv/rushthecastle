package net.maybemc.rushthecastle.map.object;

import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.attributes.Walkable;
import org.bukkit.block.Block;
import org.bukkit.util.BoundingBox;

/**
 * @author Nico_ND1
 */
public abstract class JarTeleportMapObject extends BlockMapObject implements Walkable {
    public JarTeleportMapObject(RTCGame game, Castle castle, Block block) {
        super(game, castle, block);
    }

    @Override
    public void onStep(RTCActor actor, JarSession session) {
        Block destinationBlock = getDestinationBlock();
        if (destinationBlock == null) {
            return;
        }

        BoundingBox boundingBox = destinationBlock.getBoundingBox();
        actor.teleport(destinationBlock.getLocation().add(0.5, boundingBox.getHeight(), 0.5));
    }

    public abstract Block getDestinationBlock();
}
