package net.maybemc.rushthecastle.map.object;

import com.destroystokyo.paper.ParticleBuilder;
import net.maybemc.aurelion.game.property.PropertyKey;
import net.maybemc.quests.progress.ProgressType;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.attributes.Ticking;
import net.maybemc.rushthecastle.map.object.attributes.Walkable;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.event.block.BlockPlaceEvent;

/**
 * @author Nico_ND1
 */
public class JarPortalMapObject extends BlockMapObject implements Walkable, Ticking {
    private final int id;
    private JarPortalMapObject connection;
    private final PropertyKey<Boolean> questTrackedKey;

    public JarPortalMapObject(RTCGame game, Castle castle, Block block) {
        super(game, castle, block);
        this.id = block.getType().ordinal();
        this.questTrackedKey = PropertyKey.create("portal-used-" + id, Boolean.class);
    }

    @Override
    public void onPlace(RTCActor actor, BlockPlaceEvent event) {
        for (JarPortalMapObject object : game.getMap().findObjects(JarPortalMapObject.class)) {
            if (object.castle.equals(this.castle)) {
                if (object.getId() == this.id) {
                    this.connectTo(object);
                    break;
                }
            }
        }
    }

    @Override
    public MapObjectType getType() {
        return MapObjectType.PORTAL;
    }

    @Override
    public void remove() {
        super.remove();
        disconnect();
    }

    private void disconnect() {
        if (this.connection != null) {
            this.connection.disconnect_();
            this.disconnect_();
        }
    }

    private void disconnect_() {
        this.connection = null;
    }

    public void connectTo(JarPortalMapObject other) {
        this.disconnect_();
        other.disconnect_();

        this.connectTo_(other);
        other.connectTo_(this);
    }

    private void connectTo_(JarPortalMapObject other) {
        this.connection = other;
    }

    public boolean isConnected() {
        return this.connection != null;
    }

    public int getId() {
        return id;
    }

    @Override
    public void tick(int tick) {
        if (tick % 4 != 0) {
            new ParticleBuilder(Particle.PORTAL)
                .offset(0.1D, 0.5D, 0.1D)
                .count(5)
                .extra(0.5D)
                .location(getLocation())
                .allPlayers()
                .spawn();
        }
    }

    @Override
    public void onStep(RTCActor actor, JarSession session) {
        if (this.connection != null) {
            actor.teleport(this.connection.getLocation().clone().subtract(0, 1, 0));
            actor.playSound(Sound.ENTITY_ENDERMAN_TELEPORT);

            if (actor.getProperty(questTrackedKey, false)) {
                actor.trackQuestProgress("portals-used", 1, ProgressType.INCREASE);
                actor.setProperty(questTrackedKey, true);
            }
        }
    }
}
