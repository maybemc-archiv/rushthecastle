package net.maybemc.rushthecastle.map.object;

import com.destroystokyo.paper.ParticleBuilder;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.map.castle.Castle;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

/**
 * @author Nico_ND1
 */
public class JarTeleportDownMapObject extends JarTeleportMapObject {
    public JarTeleportDownMapObject(RTCGame game, Castle castle, Block block) {
        super(game, castle, block);
    }

    @Override
    public Block getDestinationBlock() {
        Block currentBlock = this.getBlock().getRelative(BlockFace.DOWN);
        for (int i = 0; i < 20; i++) {
            Material type = currentBlock.getType();
            if (type == Material.BARRIER || !type.isSolid()) {
                currentBlock = currentBlock.getRelative(BlockFace.DOWN);
            } else {
                return currentBlock;
            }
        }
        return null;
    }

    @Override
    public MapObjectType getType() {
        return MapObjectType.TELEPORT_DOWN;
    }
}
