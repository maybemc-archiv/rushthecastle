package net.maybemc.rushthecastle.map.object;

import com.destroystokyo.paper.ParticleBuilder;
import net.maybemc.quests.progress.ProgressType;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.castle.CastleFinishEvent;
import net.maybemc.rushthecastle.map.castle.CastleTestedEvent;
import net.maybemc.rushthecastle.map.object.attributes.Ticking;
import net.maybemc.rushthecastle.map.object.attributes.Walkable;
import net.maybemc.rushthecastle.state.phase.jumping.JumpingGamePhase;
import net.maybemc.rushthecastle.state.phase.testing.TestingGamePhase;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.block.Block;

/**
 * @author Nico_ND1
 */
public class JarFinishMapObject extends BlockMapObject implements Walkable, Walkable.SessionDependent, Ticking {
    public JarFinishMapObject(RTCGame game, Castle castle, Block block) {
        super(game, castle, block);
    }

    @Override
    public void onStep(RTCActor actor, JarSession session) {
        if (game.isCurrentPhase(TestingGamePhase.class)) {
            Castle ownCastle = actor.getOwnCastle();
            if (ownCastle != null && !ownCastle.isTested()) {
                ownCastle.setTested();

                session.setEndTime(System.currentTimeMillis());
                actor.setTestTime(session.getDuration());

                Bukkit.getPluginManager().callEvent(new CastleTestedEvent(actor, ownCastle));

                actor.setSession(null);
            }
        } else if (game.isCurrentPhase(JumpingGamePhase.class)) {
            session.setEndTime(System.currentTimeMillis());

            Bukkit.getPluginManager().callEvent(new CastleFinishEvent(actor, session.getCastle()));
        }

        actor.trackQuestProgress("finishes-reached", 1, ProgressType.INCREASE);

        game.tryEnd();
    }

    @Override
    public MapObjectType getType() {
        return MapObjectType.FINISH;
    }

    @Override
    public void tick(int tick) {
        if (tick % 5 == 0) {
            new ParticleBuilder(Particle.REDSTONE)
                .offset(0.3D, 0.1D, 0.3D)
                .extra(0.1)
                .count(10)
                .location(getLocation())
                .allPlayers()
                .color(Color.RED)
                .spawn();
        }
    }
}
