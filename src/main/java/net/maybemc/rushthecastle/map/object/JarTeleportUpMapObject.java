package net.maybemc.rushthecastle.map.object;

import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.map.castle.Castle;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

/**
 * @author Nico_ND1
 */
public class JarTeleportUpMapObject extends JarTeleportMapObject {
    public JarTeleportUpMapObject(RTCGame game, Castle castle, Block block) {
        super(game, castle, block);
    }

    @Override
    public Block getDestinationBlock() {
        Block currentBlock = this.getBlock().getRelative(BlockFace.UP);
        for (int i = 0; i < 20; i++) {
            Material type = currentBlock.getType();
            if (type == Material.BARRIER || !type.isSolid()) {
                currentBlock = currentBlock.getRelative(BlockFace.UP);
            } else {
                return currentBlock;
            }
        }
        return null;
    }

    @Override
    public MapObjectType getType() {
        return MapObjectType.TELEPORT_UP;
    }
}
