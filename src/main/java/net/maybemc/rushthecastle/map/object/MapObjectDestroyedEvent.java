package net.maybemc.rushthecastle.map.object;

import net.maybemc.rushthecastle.actor.RTCActor;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

/**
 * @author Nico_ND1
 */
public class MapObjectDestroyedEvent extends Event {
    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final RTCActor actor;
    private final MapObject mapObject;

    public MapObjectDestroyedEvent(RTCActor actor, MapObject mapObject) {
        this.actor = actor;
        this.mapObject = mapObject;
    }

    public RTCActor getActor() {
        return actor;
    }

    public MapObject getMapObject() {
        return mapObject;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}
