package net.maybemc.rushthecastle.map.object;

import net.maybemc.quests.progress.ProgressType;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.attributes.Ticking;
import net.maybemc.rushthecastle.map.object.attributes.Walkable;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.List;

/**
 * @author Nico_ND1
 */
public class JarCheckpointMapObject extends BlockMapObject implements Walkable, Walkable.SessionDependent, Ticking {
    public JarCheckpointMapObject(RTCGame game, Castle castle, Block block) {
        super(game, castle, block);
    }

    @Override
    public void onStep(RTCActor actor, JarSession session) {
        if (session.addCheckpoint(getLocation())) {
            actor.playSound(Sound.BLOCK_NOTE_BLOCK_BIT);
            actor.incrementStat("checkpoints-reached");
            actor.trackQuestProgress("checkpoints-reached", 1, ProgressType.INCREASE);
        }
    }

    @Override
    public void tick(int tick) {
        if (tick % 5 == 0) {
            displayParticle();
        }
    }

    @Override
    protected boolean testParticleReceiver(RTCActor actor) {
        return actor.getSession().map(session -> {
            return !session.hasReachedCheckpoint(getLocation());
        }).orElse(true);
    }

    @Override
    public void onPlace(RTCActor actor, BlockPlaceEvent event) {
        Castle ownCastle = actor.getOwnCastle();
        List<JarCheckpointMapObject> checkpoints = ownCastle.getCheckpoints();
        actor.sendMessage("checkpoint-created", checkpoints.size(), MapObjectType.CHECKPOINT.getLimit());
    }

    @Override
    public MapObjectType getType() {
        return MapObjectType.CHECKPOINT;
    }
}
