package net.maybemc.rushthecastle.map.object;

import me.lucko.helper.Events;
import me.lucko.helper.metadata.Metadata;
import me.lucko.helper.metadata.MetadataKey;
import me.lucko.helper.terminable.Terminable;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.attributes.Ticking;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.entity.EntityChangeBlockEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
public class JarFallingAnvilMapObject extends BlockMapObject implements Ticking {
    private static final MetadataKey<Boolean> IS_FALLING_ANVIL = MetadataKey.createBooleanKey("falling-anvil");

    private final List<Terminable> terminables = new ArrayList<>();

    public JarFallingAnvilMapObject(RTCGame game, Castle castle, Block block) {
        super(game, castle, block);

        terminables.add(Events.subscribe(EntityChangeBlockEvent.class).handler(event -> {
            if (event.getBlock().equals(block)) {
                event.setCancelled(true);
            } else {
                Metadata.provideForEntity(event.getEntity()).get(IS_FALLING_ANVIL).ifPresent(isFallingAnvil -> {
                    if (isFallingAnvil) {
                        event.setCancelled(true);
                        event.getEntity().remove();
                    }
                });
            }
        }));
    }

    @Override
    public MapObjectType getType() {
        return MapObjectType.FALLING_ANVIL;
    }

    @Override
    public void remove() {
        for (Terminable terminable : terminables) {
            terminable.closeAndReportException();
        }
    }

    @Override
    public void tick(int tick) {
        if (tick % 30 == 0) {
            FallingBlock fallingBlock = location.getWorld().spawnFallingBlock(location.clone().add(0, -1, 0), Material.ANVIL.createBlockData());
            Metadata.provideForEntity(fallingBlock).put(IS_FALLING_ANVIL, true);
            fallingBlock.setDropItem(false);
            fallingBlock.setHurtEntities(true);
        }
    }
}
