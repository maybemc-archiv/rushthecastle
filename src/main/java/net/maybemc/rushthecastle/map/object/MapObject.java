package net.maybemc.rushthecastle.map.object;

import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.map.castle.Castle;
import org.bukkit.Location;

/**
 * @author Nico_ND1
 */
public abstract class MapObject {
    protected final RTCGame game;
    protected final Location location;
    protected final Castle castle;

    public MapObject(RTCGame game, Castle castle, Location location) {
        this.game = game;
        this.location = location;
        this.castle = castle;
    }

    public abstract MapObjectType getType();

    public abstract void remove();

    public Location getLocation() {
        return location;
    }

    public Castle getCastle() {
        return castle;
    }
}
