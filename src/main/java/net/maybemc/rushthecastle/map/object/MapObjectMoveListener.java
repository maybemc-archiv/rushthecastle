package net.maybemc.rushthecastle.map.object;

import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.object.attributes.Walkable;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * @author Nico_ND1
 */
public class MapObjectMoveListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    public MapObjectMoveListener(RTCGame game) {
        super(game);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Location from = event.getFrom();
        Location to = event.getTo();
        if (from.getBlockX() == to.getBlockX() && from.getBlockY() == to.getBlockY() && from.getBlockZ() == to.getBlockZ()) {
            return;
        }

        game.getMap().findMapObject(to).ifPresent(mapObject -> {
            if (mapObject instanceof Walkable walkable) {
                RTCActor actor = game.getActor(event.getPlayer());

                if (walkable instanceof Walkable.SessionDependent) {
                    actor.getSession().ifPresent(jarSession -> walkable.onStep(actor, jarSession));
                    return;
                }

                walkable.onStep(actor, null);
            }
        });
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onTeleport(PlayerTeleportEvent event) {
        onMove(new PlayerMoveEvent(event.getPlayer(), event.getFrom(), event.getTo()));
    }
}
