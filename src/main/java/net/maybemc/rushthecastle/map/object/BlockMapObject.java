package net.maybemc.rushthecastle.map.object;

import com.destroystokyo.paper.ParticleBuilder;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.castle.Castle;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.util.BoundingBox;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Nico_ND1
 */
public abstract class BlockMapObject extends MapObject {
    private final Block block;

    public BlockMapObject(RTCGame game, Castle castle, Block block) {
        super(game, castle, block.getLocation().add(0.5D, 0.0D, 0.5D));
        this.block = block;

        BoundingBox boundingBox = block.getBoundingBox();
        this.location.add(0, boundingBox.getHeight(), 0);
    }

    // Called when the event is 100% not cancelled
    public void onPlace(RTCActor actor, BlockPlaceEvent event) {
    }

    @Override
    public void remove() {
    }

    public Block getBlock() {
        return block;
    }

    protected void displayParticle() {
        new ParticleBuilder(Particle.SCRAPE)
            .offset(0.3D, 0.1D, 0.3D)
            .extra(0.1)
            .count(10)
            .location(getLocation())
            .receivers(getParticleReceivers())
            .spawn();
    }

    protected Collection<Player> getParticleReceivers() {
        List<Player> players = new ArrayList<>();
        for (RTCActor actor : game.getActors()) {
            Castle ownCastle = actor.getCurrentCastle();
            if (actor.getPlayState().isSpectator() || ownCastle == null) {
                players.add(actor.getPlayer());
            } else {
                if (ownCastle.equals(this.getCastle()) && testParticleReceiver(actor)) {
                    players.add(actor.getPlayer());
                }
            }
        }
        return players;
    }

    protected boolean testParticleReceiver(RTCActor actor) {
        return true;
    }
}
