package net.maybemc.rushthecastle.map.object;

import net.maybemc.aurelion.game.state.listener.BukkitActorEventHandler;
import net.maybemc.aurelion.game.state.listener.GameStateListenerBukkit;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.item.ItemDataTypes;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;

/**
 * @author Nico_ND1
 */
public class MapObjectGameStateListener extends GameStateListenerBukkit<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    public MapObjectGameStateListener(RTCGame game) {
        super(game);
    }

    @BukkitActorEventHandler(priority = EventPriority.HIGH, ignoreSpectator = true, ignoreCancelled = true)
    public void onBlockPlace(RTCActor actor, BlockPlaceEvent event) {
        ItemStack itemInHand = event.getItemInHand();
        if (!itemInHand.hasItemMeta()) {
            return;
        }

        Block block = event.getBlockPlaced();

        ItemMeta itemMeta = itemInHand.getItemMeta();
        PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();
        ItemDataTypes.findMapObjectCreator(dataContainer).ifPresent(mapObjectCreator -> {
            MapObjectType type = mapObjectCreator.getType();
            if (!type.canBeCreated(actor, actor.getCurrentCastle(), block.getLocation())) {
                event.setCancelled(true);
                actor.sendMessage("map-object-limit-reached", type.getLimit());
                // TODO: use an actual result and replace the previous (f.e. for Start, Finish, ... objects)
                return;
            }

            MapObject mapObject = mapObjectCreator.create(game, actor.getCurrentCastle(), block.getLocation());
            game.getMap().addMapObject(mapObject);

            if (mapObject instanceof BlockMapObject blockMapObject) {
                blockMapObject.onPlace(actor, event);
            }

            Bukkit.getPluginManager().callEvent(new MapObjectCreatedEvent(actor, mapObject));
        });
    }

    @BukkitActorEventHandler(priority = EventPriority.HIGH, ignoreSpectator = true, ignoreCancelled = true)
    public void onBlockBreak(RTCActor actor, BlockBreakEvent event) {
        Block block = event.getBlock();
        game.getMap().findMapObject(block).ifPresent(blockMapObject -> {
            blockMapObject.remove();
            game.getMap().removeMapObject(blockMapObject);

            Bukkit.getPluginManager().callEvent(new MapObjectDestroyedEvent(actor, blockMapObject));
        });
    }
}
