package net.maybemc.rushthecastle.map.object;

import com.destroystokyo.paper.ParticleBuilder;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.attributes.Ticking;
import net.maybemc.rushthecastle.map.object.attributes.Walkable;
import org.bukkit.Particle;
import org.bukkit.block.Block;

/**
 * @author Nico_ND1
 */
public class JarResetToCheckpointObject extends BlockMapObject implements Walkable, Walkable.SessionDependent, Ticking {
    public JarResetToCheckpointObject(RTCGame game, Castle castle, Block block) {
        super(game, castle, block);
    }

    @Override
    public void onStep(RTCActor actor, JarSession session) {
        session.resetToCheckpoint(actor);
    }

    @Override
    public MapObjectType getType() {
        return MapObjectType.RESET_TO_CHECKPOINT;
    }

    @Override
    public void tick(int tick) {
        if (tick % 5 == 0) {
            new ParticleBuilder(Particle.DRAGON_BREATH)
                .offset(0.3D, 0.1D, 0.3D)
                .extra(0.0015)
                .count(10)
                .location(getLocation())
                .allPlayers()
                .spawn();
        }
    }
}
