package net.maybemc.rushthecastle.map.object.attributes;

import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;

/**
 * @author Nico_ND1
 */
public interface Walkable {
    void onStep(RTCActor actor, JarSession session);

    interface SessionDependent {
    }
}
