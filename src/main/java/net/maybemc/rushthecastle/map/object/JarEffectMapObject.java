package net.maybemc.rushthecastle.map.object;

import com.destroystokyo.paper.ParticleBuilder;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.attributes.Ticking;
import net.maybemc.rushthecastle.map.object.attributes.Walkable;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.potion.PotionEffect;

/**
 * @author Nico_ND1
 */
public class JarEffectMapObject extends BlockMapObject implements Walkable, Ticking {
    private final PotionEffect effect;

    public JarEffectMapObject(RTCGame game, Castle castle, Block block, PotionEffect effect) {
        super(game, castle, block);
        this.effect = effect;
    }

    @Override
    public void onStep(RTCActor actor, JarSession session) {
        actor.getPlayer().addPotionEffect(effect);
    }

    @Override
    public void tick(int tick) {
        if (tick % 10 == 0) {
            new ParticleBuilder(Particle.DRAGON_BREATH)
                .offset(0.3D, 0.1D, 0.3D)
                .extra(0.0015)
                .count(10)
                .location(getLocation())
                .allPlayers()
                .spawn();
        }
    }

    @Override
    public MapObjectType getType() {
        return MapObjectType.EFFECT;
    }
}
