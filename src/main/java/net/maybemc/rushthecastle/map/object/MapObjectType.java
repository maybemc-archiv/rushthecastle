package net.maybemc.rushthecastle.map.object;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.castle.Castle;
import org.bukkit.Location;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Getter
public enum MapObjectType {
    START(JarStartMapObject.class, 1, null),
    FINISH(JarFinishMapObject.class, 1, null),
    CHECKPOINT(JarCheckpointMapObject.class, 25, null),
    FALLING_ANVIL(JarFallingAnvilMapObject.class, 5, null),
    EFFECT(JarEffectMapObject.class, 10, null),
    PORTAL(JarPortalMapObject.class, 20, (actor, castle, location) -> castle.findObjects(JarPortalMapObject.class).stream()
        .filter(portalMapObject -> location.getBlock().getType().ordinal() == portalMapObject.getId())
        .count() < 2),
    TELEPORT_DOWN(JarTeleportDownMapObject.class, 5, null),
    TELEPORT_UP(JarTeleportUpMapObject.class, 5, null),
    RESET_TO_CHECKPOINT(JarResetToCheckpointObject.class, 5, null);

    private final Class<? extends MapObject> clazz;
    private final int limit;
    private final Conditional conditional;

    public boolean canBeCreated(RTCActor actor, Castle castle, Location location) {
        if (limit > 0 && castle.findObjects(clazz).size() >= limit) {
            return false;
        }
        return conditional == null || conditional.canBeCreated(actor, castle, location);
    }

    public interface Conditional {
        boolean canBeCreated(RTCActor actor, Castle castle, Location location);
    }
}
