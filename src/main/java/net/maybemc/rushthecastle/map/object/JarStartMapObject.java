package net.maybemc.rushthecastle.map.object;

import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.map.castle.Castle;
import org.bukkit.block.Block;

/**
 * @author Nico_ND1
 */
public class JarStartMapObject extends BlockMapObject {
    public JarStartMapObject(RTCGame game, Castle castle, Block block) {
        super(game, castle, block);
    }

    @Override
    public MapObjectType getType() {
        return MapObjectType.START;
    }
}
