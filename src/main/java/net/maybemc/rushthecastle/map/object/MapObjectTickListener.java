package net.maybemc.rushthecastle.map.object;

import net.maybemc.aurelion.game.state.listener.GameStateListenerTask;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.team.RTCTeam;

/**
 * @author Nico_ND1
 */
public class MapObjectTickListener extends GameStateListenerTask<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private final RTCGame game;
    private int tick;

    public MapObjectTickListener(RTCGame game) {
        super(1, 1, false);
        this.game = game;
    }

    @Override
    public void run() {
        game.getMap().tick(tick++);
    }
}
