package net.maybemc.rushthecastle.map;

import net.maybemc.aurelion.game.map.GameMap;
import net.maybemc.aurelion.maps.Map;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.object.BlockMapObject;
import net.maybemc.rushthecastle.map.object.MapObject;
import net.maybemc.rushthecastle.map.object.attributes.Ticking;
import net.maybemc.rushthecastle.map.theme.ThemeConfig;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameRule;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class RTCGameMap extends GameMap<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private final List<MapObject> mapObjects;

    public RTCGameMap(@NotNull Map map, @NotNull RTCGame game) {
        super(map, game);
        this.mapObjects = new ArrayList<>();
    }

    @Override
    public @NotNull World loadWorld() {
        World world = super.loadWorld();
        world.setGameRule(GameRule.RANDOM_TICK_SPEED, 0);
        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        world.setGameRule(GameRule.DO_MOB_SPAWNING, false);
        world.setGameRule(GameRule.DO_MOB_LOOT, false);
        world.setGameRule(GameRule.KEEP_INVENTORY, false);
        world.setGameRule(GameRule.SPAWN_RADIUS, 0);

        world.setTime(6000L);
        setWorld(world);
        return world;
    }

    public void tick(int tick) {
        for (MapObject mapObject : mapObjects) {
            if (mapObject instanceof Ticking ticking) {
                ticking.tick(tick);
            }
        }
    }

    public void addMapObject(MapObject mapObject) {
        mapObjects.add(mapObject);
    }

    public void removeMapObject(MapObject mapObject) {
        mapObjects.remove(mapObject);
    }

    public Optional<MapObject> findMapObject(Location location) {
        Location loc = location.clone();
        return mapObjects.stream()
            .filter(object -> {
                Location objectLocation = object.getLocation().clone();
                return objectLocation.getBlockX() == loc.getBlockX() && Math.abs(objectLocation.getBlockY() - loc.getBlockY()) < 2.4D && objectLocation.getBlockZ() == loc.getBlockZ();
            }).findFirst();
    }

    public Optional<BlockMapObject> findMapObject(Block block) {
        return mapObjects.stream()
            .filter(mapObject -> mapObject instanceof BlockMapObject)
            .map(BlockMapObject.class::cast)
            .filter(object -> object.getBlock().equals(block)).findFirst();
    }

    public <MO extends MapObject> List<MO> findObjects(Class<MO> clazz, @Nullable Castle castle) {
        return mapObjects.stream()
            .filter(mapObject -> castle == null || mapObject.getCastle() == castle)
            .filter(clazz::isInstance)
            .map(clazz::cast)
            .collect(Collectors.toList());
    }

    public <MO extends MapObject> List<MO> findObjects(Class<MO> clazz) {
        return findObjects(clazz, null);
    }

    public List<MapObject> findObjects(Castle castle) {
        return mapObjects.stream()
            .filter(mapObject -> mapObject.getCastle() == castle)
            .collect(Collectors.toList());
    }

    public List<MapObject> getMapObjects() {
        return mapObjects;
    }

    @Override
    public @NotNull Set<RTCTeam> createTeams(@NotNull RTCGame rtcGame) {
        int maxTeams = rtcGame.variant().teams();
        Set<RTCTeam> teams = new HashSet<>(maxTeams);
        for (int i = 0; i < maxTeams; i++) {
            teams.add(new RTCTeam(ChatColor.values()[i]));
        }
        return teams;
    }
}
