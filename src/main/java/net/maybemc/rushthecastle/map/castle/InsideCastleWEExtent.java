package net.maybemc.rushthecastle.map.castle;

import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.entity.BaseEntity;
import com.sk89q.worldedit.entity.Entity;
import com.sk89q.worldedit.extent.AbstractDelegateExtent;
import com.sk89q.worldedit.extent.Extent;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.util.Location;
import com.sk89q.worldedit.world.block.BaseBlock;
import com.sk89q.worldedit.world.block.BlockState;
import com.sk89q.worldedit.world.block.BlockStateHolder;
import com.sk89q.worldedit.world.block.BlockTypes;
import org.jetbrains.annotations.Nullable;

/**
 * @author Nico_ND1
 */
// https://github.com/IntellectualSites/FastAsyncWorldEdit/issues/2121
public class InsideCastleWEExtent extends AbstractDelegateExtent {
    public static BlockState AIRSTATE = BlockTypes.AIR.getDefaultState();
    public static BaseBlock AIRBASE = BlockTypes.AIR.getDefaultState().toBaseBlock();

    private final Castle castle;

    public InsideCastleWEExtent(Extent extent, Castle castle) {
        super(extent);
        this.castle = castle;
    }

    @Override
    public BlockState getBlock(BlockVector3 position) {
        if (test(position.getX(), position.getY(), position.getZ())) {
            return super.getBlock(position);
        } else {
            return AIRSTATE;
        }
    }

    @Override
    public BaseBlock getFullBlock(BlockVector3 position) {
        if (test(position.getX(), position.getY(), position.getZ())) {
            return super.getFullBlock(position);
        } else {
            return AIRBASE;
        }
    }

    @Override
    public <T extends BlockStateHolder<T>> boolean setBlock(BlockVector3 location, T block) throws WorldEditException {
        if (test(location.getX(), location.getY(), location.getZ())) {
            return super.setBlock(location, block);
        } else {
            return false;
        }
    }

    @Nullable
    @Override
    public Entity createEntity(Location location, BaseEntity entity) {
        if (test(location.getX(), location.getY(), location.getZ())) {
            return super.createEntity(location, entity);
        } else {
            return null;
        }
    }

    private boolean test(double x, double y, double z) {
        return castle.canBeBuiltAt(x, y, z);
    }
}
