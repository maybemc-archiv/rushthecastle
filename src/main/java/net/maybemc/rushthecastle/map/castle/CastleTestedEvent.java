package net.maybemc.rushthecastle.map.castle;

import net.maybemc.rushthecastle.actor.RTCActor;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

/**
 * @author Nico_ND1
 */
public class CastleTestedEvent extends Event {
    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final RTCActor actor;
    private final Castle castle;

    public CastleTestedEvent(RTCActor actor, Castle castle) {
        this.actor = actor;
        this.castle = castle;
    }

    public RTCActor getActor() {
        return actor;
    }

    public Castle getCastle() {
        return castle;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}
