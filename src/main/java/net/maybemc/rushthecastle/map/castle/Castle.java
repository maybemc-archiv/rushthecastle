package net.maybemc.rushthecastle.map.castle;

import me.lucko.helper.scheduler.HelperExecutors;
import net.maybemc.aurelion.game.Games;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.protocol.result.Result;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.map.object.JarCheckpointMapObject;
import net.maybemc.rushthecastle.map.object.JarFinishMapObject;
import net.maybemc.rushthecastle.map.object.JarStartMapObject;
import net.maybemc.rushthecastle.map.object.MapObject;
import net.maybemc.rushthecastle.map.theme.Theme;
import net.maybemc.rushthecastle.map.theme.ThemeDecoration;
import net.maybemc.rushthecastle.schematic.Schematic;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class Castle {
    private static final AtomicInteger ID_COUNT = new AtomicInteger();

    private final int id;
    private final RTCGame game;
    private final Theme theme;
    private final List<ThemeDecoration> decorations;

    private final Location location;
    private final Vector min;
    private final Vector max;

    private Schematic schematic;
    private CompletableFuture<Result> placeSchematicFuture;

    private boolean finished = false;
    private boolean tested = false;

    public Castle(RTCGame game, Theme theme, List<ThemeDecoration> decorations, Location location) {
        this.id = ID_COUNT.incrementAndGet();
        this.game = game;
        this.theme = theme;
        this.decorations = decorations;

        this.location = location;
        this.min = theme.getRelativeBuildMin().clone().add(this.location.toVector());
        this.max = theme.getRelativeBuildMax().clone().add(this.location.toVector());
    }

    public Castle(RTCGame game, Theme theme, List<ThemeDecoration> decorations) {
        this(game, theme, decorations, new Location(game.getMap().getWorld(), 10000, 65, ID_COUNT.incrementAndGet() * 500));
        this.schematic = game.getSchematicLoader().load(theme.getId());
    }

    public JarSession createSession() {
        JarSession session = new JarSession(game, this);

        Optional<JarStartMapObject> startObjectOptional = findStartObject();
        startObjectOptional.ifPresentOrElse(startObject -> session.addCheckpoint(startObject.getLocation()), () -> session.addCheckpoint(location));

        session.setResets(0);
        return session;
    }

    public boolean canBeBuiltAt(Location location) {
        return canBeBuiltAt(location.getX(), location.getY(), location.getZ());
    }

    public boolean canBeBuiltAt(double x, double y, double z) {
        return x >= min.getX() && x <= max.getX() &&
            y >= min.getY() - 1 && y <= max.getY() &&
            z >= min.getZ() && z <= max.getZ();
    }

    public <MO extends MapObject> List<MO> findObjects(Class<MO> clazz) {
        return game.getMap().getMapObjects().stream()
            .filter(mapObject -> mapObject.getCastle() == this)
            .filter(clazz::isInstance)
            .map(clazz::cast)
            .collect(Collectors.toList());
    }

    public CompletableFuture<Result> placeSchematic() {
        if (this.placeSchematicFuture != null) {
            return this.placeSchematicFuture;
        }

        AtomicInteger decorationIndex = new AtomicInteger();
        if (schematic == null) {
            return this.placeSchematicFuture = Futures.thenCompose(CompletableFuture.completedFuture(Result.createOk()), result -> handleCompose(decorationIndex, result), 10L, TimeUnit.SECONDS);
        }
        return this.placeSchematicFuture = schematic.place(location, false).thenComposeAsync(result -> handleCompose(decorationIndex, result), Futures.asyncExecutor());
    }

    private CompletionStage<Result> handleCompose(AtomicInteger decorationIndex, Result result) {
        if (result.isOk()) {
            if (decorationIndex.get() >= decorations.size()) {
                return CompletableFuture.completedFuture(result);
            } else {
                ThemeDecoration decoration = decorations.get(decorationIndex.getAndIncrement());
                Schematic schem = game.getSchematicLoader().load(decoration.getCosmeticId());
                return schem.place(location, false).thenCompose(result1 -> handleCompose(decorationIndex, result1));
            }
        } else {
            return CompletableFuture.completedFuture(result);
        }
    }

    public Location getSpawnLocation() {
        return findStartObject().map(MapObject::getLocation).orElse(this.location);
    }

    public void teleport(RTCActor actor) {
        Location location = findStartObject().map(MapObject::getLocation).map(loc -> loc.clone().add(0.5D, 0, 0.5D)).orElse(this.location);

        if (placeSchematicFuture == null || placeSchematicFuture.isDone()) {
            actor.teleport(location);
            actor.setCurrentCastle(this);
        } else {
            placeSchematicFuture.thenAcceptAsync(result -> {
                if (result.isOk()) {
                    actor.teleport(location);
                    actor.setCurrentCastle(this);
                } else {
                    Games.getLogger().warning("Couldn't teleport actor " + actor.getUniqueId() + " to castle " + id);
                }
            }, HelperExecutors.sync());
        }
    }

    public Optional<JarStartMapObject> findStartObject() {
        return findObjects(JarStartMapObject.class).stream().findFirst();
    }

    public Optional<JarFinishMapObject> findFinishObject() {
        return findObjects(JarFinishMapObject.class).stream().findFirst();
    }

    public List<JarCheckpointMapObject> getCheckpoints() {
        return findObjects(JarCheckpointMapObject.class);
    }

    public int getId() {
        return id;
    }

    public Location getLocation() {
        return location;
    }

    public Theme getTheme() {
        return theme;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isTested() {
        return tested;
    }

    public void setTested() {
        this.tested = true;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Castle castle = (Castle) object;
        return getId() == castle.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
