package net.maybemc.rushthecastle.map.castle;

import net.maybemc.cosmetics.Cosmetic;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.map.theme.Theme;
import net.maybemc.rushthecastle.map.theme.ThemeConfig;
import net.maybemc.rushthecastle.map.theme.ThemeDecoration;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Nico_ND1
 */
public class CastleCreator {
    private final RTCGame game;
    private final Map<String, Integer> preBuiltLocationIndex;

    public CastleCreator(RTCGame game) {
        this.game = game;
        this.preBuiltLocationIndex = new HashMap<>();
    }

    public Castle create(Cosmetic cosmetic, Theme theme, List<ThemeDecoration> decorations) {
        Castle castle;
        if (theme.getPreBuiltLocation() == null) {
            castle = new Castle(game, theme, decorations);
        } else {
            castle = findLocation(cosmetic, theme.getPreBuiltLocation())
                .map(location -> new Castle(game, theme, decorations, location))
                .orElseGet(() -> new Castle(game, theme, decorations));
        }

        castle.placeSchematic();
        return castle;
    }

    private Optional<Location> findLocation(Cosmetic cosmetic, ThemeConfig.PreBuiltLocation location) {
        Vector startPosition = location.getPosition();
        int index = preBuiltLocationIndex.getOrDefault(cosmetic.getId(), 0);
        if (index >= location.getMaxLength()) {
            return Optional.empty();
        }

        preBuiltLocationIndex.put(cosmetic.getId(), index + 1);

        Location position = startPosition.toLocation(game.getMap().getWorld());

        BlockFace facing = location.getExpandingDirection();
        Vector direction = new Vector(facing.getModX(), facing.getModY(), facing.getModZ()).multiply(location.getExpandingLength());
        return Optional.of(position.add(direction.multiply(index)));
    }
}
