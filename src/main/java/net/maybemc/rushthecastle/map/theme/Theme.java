package net.maybemc.rushthecastle.map.theme;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.maybemc.cosmetics.Cosmetic;
import net.maybemc.rushthecastle.map.theme.ThemeConfig.PreBuiltLocation;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Getter
public class Theme {
    private final Cosmetic cosmetic;
    private final Vector relativeBuildMin;
    private final Vector relativeBuildMax;
    private final PreBuiltLocation preBuiltLocation;

    public String getId() {
        return cosmetic.getId();
    }

    @Nullable
    public PreBuiltLocation getPreBuiltLocation() {
        return preBuiltLocation;
    }
}
