package net.maybemc.rushthecastle.map.theme;

import lombok.Data;
import net.maybemc.aurelion.game.GameConfig;
import net.maybemc.cosmetics.Cosmetic;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * @author Nico_ND1
 */
public class ThemeConfig extends GameConfig {
    private List<Entry> entries;
    private List<DecorationEntry> decorationEntries;

    public Theme getTheme(Cosmetic cosmetic) {
        return entries.stream()
            .filter(entry -> entry.cosmeticId.equals(cosmetic.getId()))
            .findFirst()
            .map(entry -> new Theme(cosmetic, entry.relativeBuildMin, entry.relativeBuildMax, entry.preBuiltLocation))
            .orElseThrow(() -> new NullPointerException("Theme not found for cosmetic " + cosmetic.getId()));
    }

    public ThemeDecoration getThemeDecoration(String cosmeticId) {
        return decorationEntries.stream()
            .filter(entry -> entry.cosmeticId.equals(cosmeticId))
            .findFirst()
            .map(entry -> new ThemeDecoration(entry.cosmeticId))
            .orElseThrow(() -> new NullPointerException("Decoration entry not found for cosmetic " + cosmeticId));
    }

    @Data
    public static class Entry {
        private String cosmeticId;
        @Nullable
        private PreBuiltLocation preBuiltLocation;
        private Vector relativeBuildMin;
        private Vector relativeBuildMax;
    }

    @Data
    public static class PreBuiltLocation {
        private Vector position;
        private BlockFace expandingDirection;
        private int expandingLength;
        private int maxLength;
    }

    @Data
    public static class DecorationEntry {
        private String cosmeticId;
    }
}
