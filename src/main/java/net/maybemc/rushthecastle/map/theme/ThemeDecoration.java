package net.maybemc.rushthecastle.map.theme;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Nico_ND1
 */
@Data
@AllArgsConstructor
public class ThemeDecoration {
    private String cosmeticId;
}
