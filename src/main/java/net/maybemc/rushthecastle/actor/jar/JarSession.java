package net.maybemc.rushthecastle.actor.jar;

import lombok.Data;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.castle.Castle;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
@Data
public class JarSession {
    private final RTCGame game;
    private final Castle castle;
    private List<Vector> visitedCheckpoints = new ArrayList<>();
    private int resets;
    private long startTime = System.currentTimeMillis();
    private long endTime;

    public void resetToCheckpoint(RTCActor actor) {
        Location checkpoint = getCheckpoint();
        if (checkpoint == null) {
            throw new NullPointerException("Checkpoint not found for session of " + actor.getUniqueId());
        }

        actor.teleport(checkpoint.clone().add(0.5D, 0, 0.5D));
        actor.playSound(Sound.ENTITY_FISHING_BOBBER_SPLASH);
        this.resets++;
    }

    @Nullable
    public Location getCheckpoint() {
        if (visitedCheckpoints.isEmpty()) {
            return null;
        }
        return visitedCheckpoints.get(visitedCheckpoints.size() - 1).toLocation(game.getMap().getWorld());
    }

    public boolean addCheckpoint(Location location) {
        Vector blockLocation = location.getBlock().getLocation().toVector();
        if (visitedCheckpoints.contains(blockLocation)) {
            return false;
        }

        visitedCheckpoints.add(blockLocation);
        return true;
    }

    public boolean hasReachedCheckpoint(Location location) {
        return visitedCheckpoints.stream().anyMatch(checkpoint -> {
            return checkpoint.getBlockX() == location.getBlockX() && checkpoint.getBlockY() == location.getBlockY() && checkpoint.getBlockZ() == location.getBlockZ();
        });
    }

    public long getDuration() {
        return System.currentTimeMillis() - startTime;
    }

    public boolean isFinished() {
        return endTime != 0L;
    }
}
