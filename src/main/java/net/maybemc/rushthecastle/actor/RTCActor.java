package net.maybemc.rushthecastle.actor;

import me.lucko.helper.scheduler.HelperExecutors;
import net.maybemc.aurelion.game.actor.Actor;
import net.maybemc.cosmetics.Cosmetic;
import net.maybemc.cosmetics.CosmeticRepository;
import net.maybemc.cosmetics.price.NonePrice;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.maybemc.quests.QuestsPlugin;
import net.maybemc.quests.progress.ProgressType;
import net.maybemc.rushthecastle.RTCGame;
import net.maybemc.rushthecastle.RewardConfig;
import net.maybemc.rushthecastle.actor.jar.JarSession;
import net.maybemc.rushthecastle.map.castle.Castle;
import net.maybemc.rushthecastle.map.castle.CastleFinishEvent;
import net.maybemc.rushthecastle.map.theme.Theme;
import net.maybemc.rushthecastle.map.theme.ThemeConfig;
import net.maybemc.rushthecastle.map.theme.ThemeDecoration;
import net.maybemc.rushthecastle.state.phase.building.BuildingGamePhase;
import net.maybemc.rushthecastle.team.RTCTeam;
import net.maybemc.shop.product.Product;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * @author Nico_ND1
 */
public class RTCActor extends Actor<RTCActor, RTCTeam> {
    private final RTCGame game;
    private Castle ownCastle;
    private Castle currentCastle;
    private JarSession session;

    private long testTime;

    public RTCActor(@NotNull RTCGame game, @NotNull Player player) {
        super(game, player);
        this.game = game;
    }

    @Override
    public void makePlaying() {
    }

    @Override
    public void makeSpectator() {
        super.makeSpectator();

        if (ownCastle != null) {
            sendTitle("eliminated-title", "eliminated-subtitle", 5, 60, 14);
        }
    }

    @Override
    public boolean canRespawn() {
        return true;
    }

    @Override
    protected GameMode getGameMode() {
        if (game.isCurrentPhase(BuildingGamePhase.class)) {
            return GameMode.CREATIVE;
        }
        return GameMode.ADVENTURE;
    }

    public void finishBuilding() {
        if (ownCastle.isFinished()) {
            return;
        }

        ownCastle.setFinished(true);
        clearPlayer();
        getPlayer().setAllowFlight(true);
        game.getCurrentState().tryGiveItems(this);

        Bukkit.getPluginManager().callEvent(new CastleFinishEvent(this, ownCastle));
    }

    public void unfinishBuilding() {
        if (!ownCastle.isFinished()) {
            return;
        }

        ownCastle.setFinished(false);
        clearPlayer();
        getPlayer().setAllowFlight(true);
        game.getCurrentState().tryGiveItems(this);

        Bukkit.getPluginManager().callEvent(new CastleFinishEvent(this, ownCastle));

        for (RTCActor rtcActor : game.getActorsPlaying()) {
            Nick nick = this.getNickFor(rtcActor);
            rtcActor.sendMessage("building-unfinished", nick.getDisplayName());
            rtcActor.playSound(Sound.BLOCK_NOTE_BLOCK_BASS);
        }
    }

    public Castle getOwnCastle() {
        return ownCastle;
    }

    public void loadOwnCastle(RTCGame game) {
        if (ownCastle != null) {
            return;
        }

        findCosmeticInventory().ifPresentOrElse(inventory -> {
            Optional<Cosmetic> selectedCosmetic = inventory.getSelectedCosmetic("rtc-theme");
            Cosmetic cosmetic = selectedCosmetic.orElseGet(() -> {
                CosmeticRepository cosmeticRepository = Meybee.getInstance().getProvider(CosmeticRepository.class);
                for (Cosmetic allCosmetic : cosmeticRepository.allCosmetics()) {
                    if ("rtc-theme".equals(allCosmetic.getCategory()) && allCosmetic.getPrice() instanceof NonePrice) {
                        return allCosmetic;
                    }
                }
                throw new IllegalStateException();
            });

            ThemeConfig themeConfig = game.getThemeConfig();
            Theme theme = themeConfig.getTheme(cosmetic);

            List<ThemeDecoration> decorations = new ArrayList<>();
            Optional<Cosmetic> selectedFloor = inventory.getSelectedCosmetic("rtc-theme-floor");
            selectedFloor.ifPresent(c -> decorations.add(themeConfig.getThemeDecoration(c.getId())));

            ownCastle = game.getCastleCreator().create(cosmetic, theme, decorations);
        }, () -> {
            throw new IllegalStateException("No cosmetic inventory found for " + getUniqueId() + " to load own castle");
        });
    }

    public Castle getCurrentCastle() {
        return currentCastle;
    }

    public void setCurrentCastle(Castle currentCastle) {
        this.currentCastle = currentCastle;
    }

    public Optional<JarSession> getSession() {
        return Optional.ofNullable(session);
    }

    public void operateSession(Consumer<JarSession> consumer) {
        if (session != null) {
            consumer.accept(session);
        }
    }

    public void setSession(JarSession session) {
        this.session = session;
    }

    public int getPoints() {
        return getTeam().getPoints();
    }

    public void addPoints(int points) {
        getTeam().addPoints(points);
    }

    public long getTestTime() {
        return testTime;
    }

    public void setTestTime(long testTime) {
        this.testTime = testTime;
    }

    public void makeSpectatorTemporary() {
        Player player = getPlayer();
        for (RTCActor a : game.getActorsPlaying()) {
            a.getPlayer().hidePlayer(game.getPlugin(), player);
            player.showPlayer(game.getPlugin(), a.getPlayer());
        }

        player.setGameMode(GameMode.ADVENTURE);
        player.setAllowFlight(true);
        player.setFlying(true);
        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0, false, false, false));
    }

    public void teleport(Location location) {
        Player player = getPlayer();
        Location playerLocation = player.getLocation();

        location = location.clone();
        location.setYaw(playerLocation.getYaw());
        location.setPitch(playerLocation.getPitch());

        player.teleport(location);
    }

    public void reward(RewardConfig.Action action, String arg) {
        Product reward = game.getRewardConfig().getReward(action, arg);
        if (reward != null) {
            giveReward(action, arg, reward);
        }
    }

    public void reward(RewardConfig.Action action) {
        Product reward = game.getRewardConfig().getReward(action);
        if (reward != null) {
            giveReward(action, null, reward);
        }
    }

    private void giveReward(RewardConfig.Action action, String arg, Product product) {
        Futures.addCallback(product.giveTo(getUser()), new FutureCallback<>() {
            @Override
            public void onSuccess(Product.GiveResult giveResult) {
                if (giveResult.isSuccess()) {
                    sendMessage("reward-give-success-" + action.name(), product.translateName(getUser()), arg);
                } else {
                    sendMessage("reward-give-error", product.translateName(getUser()));
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                FutureCallback.super.onFailure(throwable);
                sendMessage("reward-give-error", product.translateName(getUser()));
            }
        }, HelperExecutors.sync(), 10L, TimeUnit.SECONDS);
    }

    public void trackQuestProgress(String key, int amount, ProgressType progressType) {
        QuestsPlugin questsPlugin = JavaPlugin.getPlugin(QuestsPlugin.class);
        questsPlugin.progress(getUser(), key, amount, progressType);
    }

    public void trackQuestProgress(String key, long amount, ProgressType progressType) {
        QuestsPlugin questsPlugin = JavaPlugin.getPlugin(QuestsPlugin.class);
        questsPlugin.progress(getUser(), key, amount, progressType);
    }
}
