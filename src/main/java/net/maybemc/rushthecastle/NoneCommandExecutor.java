package net.maybemc.rushthecastle;

import net.maybemc.rushthecastle.actor.RTCActor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

/**
 * @author Nico_ND1
 */
public class NoneCommandExecutor implements CommandExecutor {
    private final RTCGame game;

    public NoneCommandExecutor(RTCGame game) {
        this.game = game;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        RTCActor actor = game.getActor((Player) sender);
        actor.sendMessage("command-disabled");
        return true;
    }
}
