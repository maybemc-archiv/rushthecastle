package net.maybemc.rushthecastle;

import net.maybemc.aurelion.game.GameConfig;

/**
 * @author Nico_ND1
 */
public class RTCGameConfig extends GameConfig {
    private int playersPerTeam;
    private int teams;

    public int getPlayersPerTeam() {
        return playersPerTeam;
    }

    public int getTeams() {
        return teams;
    }
}
