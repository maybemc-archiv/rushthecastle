package net.maybemc.rushthecastle;

import lombok.Getter;
import lombok.Setter;
import net.maybemc.aurelion.game.Game;
import net.maybemc.aurelion.game.map.GameVariant;
import net.maybemc.aurelion.game.map.LobbyGameMap;
import net.maybemc.aurelion.game.state.GameState;
import net.maybemc.aurelion.maps.Map;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.i18n.LanguageRepository;
import net.maybemc.meybee.api.json.GsonProvider;
import net.maybemc.meybee.spigot.tablist.GroupTablist;
import net.maybemc.meybee.spigot.tablist.Tablist;
import net.maybemc.meybee.spigot.tablist.TablistRepository;
import net.maybemc.rushthecastle.actor.RTCActor;
import net.maybemc.rushthecastle.map.RTCGameMap;
import net.maybemc.rushthecastle.map.castle.CastleCreator;
import net.maybemc.rushthecastle.map.theme.ThemeConfig;
import net.maybemc.rushthecastle.schematic.SchematicLoader;
import net.maybemc.rushthecastle.schematic.SchematicLoaderWorldEdit;
import net.maybemc.rushthecastle.state.lobby.RTCLobbyGameMap;
import net.maybemc.rushthecastle.state.lobby.RTCLobbyGameState;
import net.maybemc.rushthecastle.state.lobby.timevoting.TimeVotingConfig;
import net.maybemc.rushthecastle.state.lobby.timevoting.TimeVotingItem;
import net.maybemc.rushthecastle.state.phase.RTCPlayingGroupTablist;
import net.maybemc.rushthecastle.state.phase.building.menu.BuildingMenuConfig;
import net.maybemc.rushthecastle.state.phase.jumping.PointsConfig;
import net.maybemc.rushthecastle.team.RTCTeam;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
@Getter
// TODO: if game is force started we don't end on phase transition
public class RTCGame extends Game<RTCActor, RTCTeam, RTCGameMap, RTCGame> {
    private final RTCGameConfig config;
    private final ThemeConfig themeConfig;
    private final SchematicLoader schematicLoader;
    private final BuildingMenuConfig buildingMenuConfig;
    private final CastleCreator castleCreator;
    private final PointsConfig pointsConfig;
    private RewardConfig rewardConfig;
    private final TimeVotingConfig timeVotingConfig;

    @Setter
    private int buildingTime;
    @Setter
    private int jumpingTime;

    public RTCGame(@NotNull JavaPlugin plugin) throws IOException, InstantiationException, IllegalAccessException {
        super(plugin, "rushthecastle");
        this.config = loadConfig(RTCGameConfig.class);
        this.themeConfig = loadConfig("meybee/config/themes.json", ThemeConfig.class);
        this.buildingMenuConfig = loadConfig("meybee/config/building-menu.json", BuildingMenuConfig.class);
        this.pointsConfig = loadConfig("meybee/config/points.json", PointsConfig.class);
        this.timeVotingConfig = loadConfig("meybee/config/time-voting.json", TimeVotingConfig.class);

        this.schematicLoader = new SchematicLoaderWorldEdit();
        this.castleCreator = new CastleCreator(this);

        enableStats("rushthecastle", pointsConfig.getStatScoreWeighting());
    }

    @Override
    protected LanguagePack loadLanguagePack() {
        return Meybee.getInstance().getProvider(LanguageRepository.class).loadPacks("aurelion", "games/rushthecastle", "cosmetics");
    }

    @Override
    public @NotNull GameVariant variant() {
        return GameVariant.of("default", config.getPlayersPerTeam(), config.getTeams());
    }

    @Override
    protected @NotNull RTCGameMap createGameMap(@NotNull Map map, @NotNull RTCGame rtcGame) {
        return new RTCGameMap(map, rtcGame);
    }

    @Override
    protected LobbyGameMap<RTCActor, RTCTeam, RTCGameMap, RTCGame> createLobbyMap(Map map) {
        return new RTCLobbyGameMap(map, this);
    }

    @Override
    protected void load() {
        getMap().loadWorld();
    }

    @Override
    protected @NotNull GameState<RTCActor, RTCTeam, RTCGameMap, RTCGame> getInitialState() {
        return new RTCLobbyGameState(this);
    }

    @Override
    protected void registerActor(@NotNull RTCActor rtcActor) {
    }

    @Override
    protected void unregisterActor(@NotNull RTCActor rtcActor) {
    }

    @Override
    protected @NotNull RTCActor createActor(@NotNull Player player) {
        return new RTCActor(this, player);
    }

    @Override
    protected Tablist createGroupTablist() {
        return new RTCPlayingGroupTablist();
    }

    @Override
    protected void assignTeamTablist() {
        assignGroupTablist();
    }

    public RewardConfig getRewardConfig() {
        if (rewardConfig == null) {
            try {
                this.rewardConfig = Meybee.getInstance().getProvider(GsonProvider.class).preLoadAndGetConfig(RewardConfig.class);
            } catch (IOException | InstantiationException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return rewardConfig;
    }

    public List<RTCActor> getTopActors() {
        return getActorsPlaying().stream().sorted(Comparator.comparingInt(RTCActor::getPoints).reversed()).collect(Collectors.toList());
    }
}
